#
#-*-coding:utf-8-*-
#
from gtk import *
import pango
import time
from DatePicker import DatePicker
from Config import ProxyConfig
from Notas import Renderer

class Agenda(Frame):
    def __init__(self,parent):
        self.top=parent
        self.db = parent.db
        Frame.__init__(self)
        self.menubar = MenuBar()
        self.make_menu()
        self.web = self.top.web
        base = VBox(False,False)
        bx = HBox(False,0)        
        base.pack_start(self.menubar,False,True)
        base.pack_end(bx,True,True)
        fr1 = Frame("Compromissos:")
        ibx = VBox(False,0)
        fr2 = Frame("Pendencias:")
        scw = ScrolledWindow()
        scw.set_policy(POLICY_AUTOMATIC,POLICY_AUTOMATIC)
        self.compromissos = TreeView()
        self.compromissos.connect("row-activated",self.abre_compromisso)
        col=TreeViewColumn("ID",Renderer(),text=0,foreground=3)
	col.set_visible(False)
        self.compromissos.append_column(col)
        self.compromissos.append_column(TreeViewColumn("Data",Renderer(),text=1,foreground=3))
        self.compromissos.append_column(TreeViewColumn("Descrição",Renderer(),text=2,foreground=3))
        self.compromissos.get_columns()[0].set_fixed_width(300)
        scw.add(self.compromissos)
        ibx.pack_start(scw)        
        frt = Frame()
        self.data= DatePicker()
        ibx2 = HBox(False,0)
        ibx2.pack_start(self.data,True,True)
        bt = ToolButton('gtk-refresh')
        bt.connect("clicked",self.atualiza_compromissos)
        ibx2.pack_end(bt,False,False)
        ibx.pack_end(ibx2,False,False)
        fr1.add(ibx)
        scw = ScrolledWindow()
        scw.set_policy(POLICY_AUTOMATIC,POLICY_AUTOMATIC)        
        self.data.set(time.strftime("%Y-%m-%d"))
        self.pendencias = TreeView()
        self.pendencias.connect("row-activated",self.abrir_pendencia)
        col=TreeViewColumn("Id",Renderer(),text=0,foreground=2)
        col.set_visible(False)
        self.pendencias.append_column(col)
        self.pendencias.append_column(TreeViewColumn("Descrição",Renderer(),text=1,foreground=2))
        self.pendencias.set_size_request(200,-1)
        scw.add(self.pendencias)
        fr2.add(scw)
        bx.pack_start(fr1,True,True)
        bx.pack_end(fr2,False,True)
        self.add(base)
        self.atualiza_compromissos()

    def make_menu(self):
        mmenu = Menu()
        imenu = Menu()
        
        iitens = [ImageMenuItem("gtk-refresh"),SeparatorMenuItem(),MenuItem("Tornar compromisso local"),MenuItem("Tornar pendencia local"),SeparatorMenuItem(),MenuItem("Enviar compromisso local para internet"),MenuItem("Enviar pendencia local para internet"),SeparatorMenuItem(),MenuItem("Configurar proxy")]
        for i in iitens:imenu.append(i)
        iitens[8].connect("activate",lambda evt:ProxyConfig(self))        
        iitens[6].connect("activate",lambda evt:self.pendencia2web())        
        iitens[5].connect("activate",lambda evt:self.compromisso2web())
        iitens[3].connect("activate",lambda evt:self.web2pendencia())                
        iitens[2].connect("activate",lambda evt:self.web2compromisso())        
        iitens[0].connect("activate",lambda evt:self.AtualizaCompromissosInternet())
        itens = [ MenuItem("Internet"),SeparatorMenuItem(),MenuItem("Adicionar compromisso"),MenuItem("Baixar compromisso"),SeparatorMenuItem(),
                  MenuItem("Adicionar pendencia"),MenuItem("Baixar pendencia")]              
        for i in itens[0:4]:mmenu.append(i)
        itens[0].set_submenu(imenu)
        itens[2].connect("activate",self.novo_compromisso)
        itens[3].connect("activate",self.baixar_compromisso)
        itens[5].connect("activate",self.nova_pendencia)
        itens[6].connect("activate",self.baixa_pendencia)
#        itens[8].connect("activate",lambda evt:self.top.destroy())
        mp = Menu()
        for i in itens[5:]:mp.append(i)#Cria um menu separador para as pendencias
        m = MenuItem("Agenda")
        m.set_submenu(mmenu)
        m2 = MenuItem("Pendencias")
        m2.set_submenu(mp)
        m2.set_right_justified(True)
        self.menubar.insert(m,0)
        self.menubar.insert(m2,1)

    def atualiza_compromissos(self,evt=None):
        md = ListStore(str,str,str,str)
        hoje= self.data.get()
        for cp in self.db.execute("SELECT id,data,descricao,'#0000FF' FROM compromissos WHERE data<=? AND pendente='1'",(hoje,)).fetchall():
            md.append(cp)
        self.compromissos.set_model(md)
        md = ListStore(str,str,str)
        for cp in self.db.execute("SELECT id,descricao,'#0000FF' FROM pendencias WHERE pendente='1'").fetchall():
            md.append(cp)
        self.pendencias.set_model(md)
        
    def AtualizaCompromissosInternet(self):
        self.atualiza_compromissos()#limpa a lista de outras possiveis atualizacoes via iternet
        md = self.compromissos.get_model()
        inf = {'usuario':self.db.execute("SELECT valor FROM config WHERE chave='usuario'").fetchone()[0],
               'senha':self.db.execute("SELECT valor FROM config WHERE chave='senha'").fetchone()[0],'data':self.data.get()}
        dados=self.top.web.listar_compromissos(inf)
        for ln in dados:
            linha = ['W',ln['data'],ln['descricao'],'#00FF00'] #W para compromissos baixados da internet ou seja sem ID
            md.append(linha)
        md = self.pendencias.get_model()
        inf.pop('data');inf['op']='listar'
        dados=self.top.web.listar_pendencias(inf)
        for ln in dados:md.append([ln['descricao'],'#00FF00'])
        

    def novo_compromisso(self,evt=None):
        dlg = DLGCompromisso(self)
        if dlg.run():
            self.db.execute('INSERT INTO compromissos(data,descricao,pendente,obs) VALUES(?,?,?,?)',
                (dlg.data.get(),dlg.descricao.get_text(),1,dlg.get_obs()))
            self.db.commit()
            self.atualiza_compromissos()
        dlg.destroy()

    def baixar_compromisso(self,evt):
        md,it = self.compromissos.get_selection().get_selected()
        if it==None:return
        data = md.get_value(it,0)
        descricao =md.get_value(it,1)
        self.db.execute("UPDATE compromissos SET pendente='0' WHERE data=? AND descricao=?",(data,descricao))
        self.db.commit()
        self.atualiza_compromissos()

    def abre_compromisso(self,evt,obj,idx):
        md,it = self.compromissos.get_selection().get_selected()
        if it==None:return
        if md.get_value(it,2)=='#00FF00':
            data,descricao,obs= self.top.web.abrir_compromisso({'usuario':self.db.execute("SELECT valor FROM config WHERE chave='usuario'").fetchone()[0],
                                                                'senha':self.db.execute("SELECT valor FROM config WHERE chave='senha'").fetchone()[0],
                                                                'data':md.get_value(it,0),
                                                                'descricao':md.get_value(it,1)})
        else:
            id=md.get_value(it,0)
            data = md.get_value(it,1)
            descricao =md.get_value(it,2)
            obs = self.db.execute('SELECT obs FROM compromissos WHERE id=?',(id,)).fetchone()[0]
        if not(obs):obs=''
        dlg = DLGCompromisso(self)
        bt = dlg.add_button("gtk-go-down",2)
        bt.get_child().get_children()[0].get_children()[1].set_text("Baixar")
        dlg.descricao.set_text(descricao)
        dlg.obs.get_buffer().set_text(obs)
        ret = dlg.run()
        if (md.get_value(it,2))=="#00FF00":
            if ret==2:
                self.top.web.apagar_compromisso({'usuario':self.db.execute("SELECT valor FROM config WHERE chave='usuario'").fetchone()[0],
                                                'senha':self.db.execute("SELECT valor FROM config WHERE chave='senha'").fetchone()[0],
                                                'data':self.data.get(),
                                                'descricao':md.get_value(it,1)})
            if ret==1:
                self.top.web.salvar_compromisso({'usuario':self.db.execute("SELECT valor FROM config WHERE chave='usuario'").fetchone()[0],
                                                'senha':self.db.execute("SELECT valor FROM config WHERE chave='senha'").fetchone()[0],
                                                'data':dlg.data.get(),
                                                'descricao':dlg.descricao.get_text(),
                                                'obs':dlg.get_obs(),
                                                'data_original':data,
                                                'descricao_original':descricao})                                                                                
            if ret!=0:self.AtualizaCompromissosInternet();#Evitar perder tempo quando for selecionado o cancelar
        else:           
            if ret==2:
                self.db.execute("UPDATE compromissos SET pendente='0' WHERE id=?",(id,))
            if ret==1:
                self.db.execute("UPDATE compromissos SET obs=?, data=?,descricao=? WHERE id=?",
                (dlg.get_obs(),dlg.data.get(),dlg.descricao.get_text(),id))
            if ret!=0:
                self.db.commit()
            self.atualiza_compromissos()
        dlg.destroy()

    def baixa_pendencia(self,evt=None):
        md,it = self.pendencias.get_selection().get_selected()
        if it==None:return
        id =md.get_value(it,0)
        self.db.execute("UPDATE pendencias SET pendente='0' WHERE id=?",(id,))
        self.db.commit()
        self.atualiza_compromissos()

    def nova_pendencia(self,evt=None):
        dlg = DLGPendencia(self)
        if dlg.run():
            self.db.execute("INSERT INTO pendencias(descricao,obs,pendente) VALUES(?,?,1)",(dlg.descricao.get_text(),dlg.get_obs()))
            self.db.commit()
            self.atualiza_compromissos()
        dlg.destroy()

    def abrir_pendencia(self,evt,idx,obj):
        md,it = self.pendencias.get_selection().get_selected()
        if it==None:return
        if md.get_value(it,1)=="#00FF00":
            dados = {'usuario':self.db.execute("SELECT valor FROM config WHERE chave='usuario'").fetchone()[0],
                     'senha':self.db.execute("SELECT valor FROM config WHERE chave='senha'").fetchone()[0],
                     'descricao':md.get_value(it,0)}
            descricao,obs=self.top.web.abrir_pendencia(dados)
        else:
            id=md.get_value(it,0)
            descricao = md.get_value(it,1)
            obs = self.db.execute('SELECT obs FROM pendencias WHERE id=?',(id,)).fetchone()[0]            
        dlg = DLGPendencia(self)
        dlg.descricao.set_text(descricao)
        dlg.obs.get_buffer().set_text(obs)
        bt=dlg.add_button("gtk-go-down",2)
        bt.get_child().get_children()[0].get_children()[1].set_text("Baixar")
        ret=dlg.run()
        if md.get_value(it,1)=="#00FF00":
            if ret==2:
                dados = {'usuario':self.db.execute("SELECT valor FROM config WHERE chave='usuario'").fetchone()[0],
                     'senha':self.db.execute("SELECT valor FROM config WHERE chave='senha'").fetchone()[0],
                     'descricao':md.get_value(it,0)}
                self.top.web.apagar_pendencia(dados)
            if ret==1:
                dados = {'usuario':self.db.execute("SELECT valor FROM config WHERE chave='usuario'").fetchone()[0],
                     'senha':self.db.execute("SELECT valor FROM config WHERE chave='senha'").fetchone()[0],
                     'descricao_velha':md.get_value(it,0),
                     'descricao':dlg.descricao.get_text(),
                     'obs':dlg.get_obs()}
                self.top.web.salvar_pendencia(dados)
            
            if ret!=0:self.AtualizaCompromissosInternet()
        else:
            if ret==2:
                self.db.execute("UPDATE pendencias SET pendente='0' WHERE id=?",(id,))
            if ret==1:
                self.db.execute("UPDATE pendencias SET descricao=?,obs=? WHERE id=?",(dlg.descricao.get_text(),dlg.get_obs(),id))
            if ret!=0:
                self.db.commit()
                self.atualiza_compromissos()
        dlg.destroy()

    def web2compromisso(self):
        md,it = self.compromissos.get_selection().get_selected()
        if it==None:return
        if md.get_value(it,2)!='#00FF00':return#ja e local
        data,descricao,obs = self.top.web.abrir_compromisso({'usuario':self.db.execute("SELECT valor FROM config WHERE chave='usuario'").fetchone()[0],
                                                                'senha':self.db.execute("SELECT valor FROM config WHERE chave='senha'").fetchone()[0],
                                                                'data':self.data.get(),
                                                                'descricao':md.get_value(it,1)})
        if self.db.execute("SELECT count(descricao) FROM compromissos WHERE descricao=? AND data=?",(descricao,data)).fetchone()[0]>0:
            self.top.alert("Registro já existe localmente")
        else:
            self.db.execute("INSERT INTO compromissos(data,descricao,obs,pendente) VALUES(?,?,?,'1')",(data,descricao,obs))
            self.db.commit()
            self.AtualizaCompromissosInternet()
        
    def web2pendencia(self):
        md,it = self.pendencias.get_selection().get_selected()
        if it==None:return
        if md.get_value(it,1)!='#00FF00':return#ja e local
        descricao,obs = self.top.web.abrir_pendencia({'usuario':self.db.execute("SELECT valor FROM config WHERE chave='usuario'").fetchone()[0],
                                                                'senha':self.db.execute("SELECT valor FROM config WHERE chave='senha'").fetchone()[0],
                                                                'descricao':md.get_value(it,0)})
        if self.db.execute("SELECT count(descricao) FROM pendencias WHERE descricao=?",(descricao,)).fetchone()[0]>0:
            self.top.alert("Registro já existe localmente")
        else:
            self.db.execute("INSERT INTO pendencias(descricao,obs,pendente) VALUES(?,?,'1')",(descricao,obs))
            self.db.commit()
            self.AtualizaCompromissosInternet()
    
    def compromisso2web(self):
        md,it = self.compromissos.get_selection().get_selected()
        if it==None:return
        if md.get_value(it,2)=='#00FF00':return#ja e web
        dados={'data':md.get_value(it,0),
               'descricao':md.get_value(it,1),
               'obs':self.db.execute("SELECT obs FROM compromissos WHERE data=? AND descricao=?",(md.get_value(it,0),md.get_value(it,1))).fetchone()[0],
               'usuario':self.db.execute("SELECT valor FROM config WHERE chave='usuario'").fetchone()[0],
               'senha':self.db.execute("SELECT valor FROM config WHERE chave='senha'").fetchone()[0]}
        self.top.web.salvar_compromisso(dados)
        
    
    def pendencia2web(self):
        md,it = self.pendencias.get_selection().get_selected()
        if it==None:return
        if md.get_value(it,1)=='#00FF00':return#ja e web
        descricao,obs = self.db.execute("SELECT descricao,obs FROM pendencias WHERE descricao=?",(md.get_value(it,0),)).fetchone()
        dados= {'descricao':descricao,'obs':obs,
               'usuario':self.db.execute("SELECT valor FROM config WHERE chave='usuario'").fetchone()[0],
               'senha':self.db.execute("SELECT valor FROM config WHERE chave='senha'").fetchone()[0]}
        self.top.web.salvar_pendencia(dados)


class DLGCompromisso(Dialog):
        def __init__(self,parent):
            Dialog.__init__(self)
            self.set_title("Compromisso")
            self.data= DatePicker("Data:")
            self.data.set(parent.data.get())
            fr = Frame("Descrição:")
            self.descricao = Entry()
            fr.add(self.descricao)
            self.obs = TextView()
            self.obs.modify_font(pango.FontDescription('courier new 10'))
            fr2=Frame("Observações:")
            scw = ScrolledWindow()
            scw.set_policy(POLICY_AUTOMATIC,POLICY_AUTOMATIC)
            scw.add(self.obs)
            fr2.add(scw)
            self.vbox.pack_start(self.data,False,True)
            self.vbox.pack_start(fr,False,True)
            self.vbox.pack_end(fr2,True,True)
            self.add_button("gtk-ok",1)
            self.add_button("gtk-cancel",0)
            self.set_size_request(400,300)
            self.show_all()

        def get_obs(self):
            bf = self.obs.get_buffer()
            ini =bf.get_start_iter()
            end = bf.get_end_iter()
            return(bf.get_text(ini,end))

class DLGPendencia(Dialog):
        def __init__(self,parent):
            Dialog.__init__(self)
            self.set_title("Pendencia")
            fr = Frame("Descrição:")
            self.descricao = Entry()
            fr.add(self.descricao)
            self.obs = TextView()
            self.obs.modify_font(pango.FontDescription('courier new 10'))
            fr2=Frame("Observações:")
            scw = ScrolledWindow()
            scw.set_policy(POLICY_AUTOMATIC,POLICY_AUTOMATIC)
            scw.add(self.obs)
            fr2.add(scw)
            self.vbox.pack_start(fr,False,True)
            self.vbox.pack_end(fr2,True,True)
            self.add_button("gtk-ok",1)
            self.add_button("gtk-cancel",0)
            self.set_size_request(300,300)
            self.show_all()

        def get_obs(self):
            bf = self.obs.get_buffer()
            ini =bf.get_start_iter()
            end = bf.get_end_iter()
            return(bf.get_text(ini,end))

if __name__=="__main__":execfile("MainWindow.py",{"__name__":"__main__"})
