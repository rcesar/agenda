#
#-*-coding:utf-8-*-
#

from gtk import *
import webbrowser as browser
from json import JSONEncoder
from json import JSONDecoder

class Favoritos(Frame):
    def __init__(self,parent):
        self.db = parent.db
        self.top=parent
        Frame.__init__(self)
        self.lista=TreeView()
        tb = Toolbar()
        bts = [ ToolButton('gtk-new'),ToolButton('gtk-edit'),SeparatorToolItem(),ToolButton('gtk-delete'),SeparatorToolItem(),ToolButton('gtk-save'),ToolButton('gtk-open')]
        bts[-2].set_label("Exportar Favoritos")
        bts[-1].set_label("Importar Favoritos")
        for bt in bts:tb.add(bt)
        bx = VBox()
        scw = ScrolledWindow()
        scw.set_policy(POLICY_AUTOMATIC,POLICY_AUTOMATIC)
        scw.add(self.lista)
        bx.pack_start(tb,False,True)
        bx.pack_end(scw,True,True)
        self.lista.set_headers_visible(False)
        self.lista.append_column(TreeViewColumn("Categoria",CellRendererText(),text=0))
        self.lista.append_column(TreeViewColumn("Descrição",CellRendererText(),text=1))
        self.lista.append_column(TreeViewColumn("URL",CellRendererText(),text=2))
        self.add(bx)
        self.atualizar()
        #connect
        bts[0].connect("clicked",self.novo)
        bts[1].connect("clicked",self.edit)
        bts[3].connect("clicked",self.delete)
        bts[5].connect("clicked",self.exportar)
        bts[6].connect("clicked",self.importar)
        self.lista.connect("row-activated",self.abrir)

    def atualizar(self):
        md =TreeStore(str,str,str,str)
        for categoria in self.db.execute('SELECT categoria FROM favoritos GROUP BY categoria').fetchall():
            it = md.append(None,[categoria[0],'','',''])
            for item in self.db.execute("SELECT descricao,url FROM favoritos WHERE categoria=? ORDER BY descricao",categoria).fetchall():
                md.append(it,[item[0],item[1],'',categoria[0]])
        self.lista.set_model(md)

    def novo(self,evt):
        dlg = DLGFav(self.top)
        if dlg.run():
            try:
                self.db.execute("INSERT INTO favoritos VALUES(?,?,?)",(dlg.categoria.get_text(),dlg.descricao.get_text(),dlg.url.get_text()))
                self.db.commit();self.atualizar()
            except Exception,e:print(e)
        dlg.destroy()

    def edit(self,evt):
        it,idx = self.lista.get_selection().get_selected()
        if idx==None:return
        if it[idx][0]=='':return#categoria
        dlg = DLGFav(self.top)
        dlg.categoria.set_text(it[idx][3])
        dlg.descricao.set_text(it[idx][0])
        dlg.url.set_text(it[idx][1])        
        if dlg.run():
            try:
                self.db.execute("UPDATE favoritos SET categoria=?,descricao=?,url=? WHERE categoria=? AND descricao=? AND url=?",(dlg.categoria.get_text(),dlg.descricao.get_text(),
                                                                                                                                  dlg.url.get_text(),it[idx][3],it[idx][0],it[idx][1]))
                self.db.commit();self.atualizar()
            except Exception,e:print(e)
        dlg.destroy()

    def delete(self,evt):
        it,idx = self.lista.get_selection().get_selected()
        if idx==None:return
        if it[idx][3]=='':return
        dlg = MessageDialog(buttons=BUTTONS_YES_NO,parent=self.top)
        dlg.set_markup("Apagar o favorito:\nCategoria:%s\nDescrição:%s\nURL:%s"%(it[idx][3],it[idx][0],it[idx][1]))
        if dlg.run()==RESPONSE_YES:
            try:
                self.db.execute("DELETE FROM favoritos WHERE categoria=? AND descricao=? AND url=?",(it[idx][3],it[idx][0],it[idx][1]))
                self.db.commit()
                self.atualizar()
            except Exception,e:print(e)
        dlg.destroy()

    def abrir(self,evt,idx,cln):
        it,idx = self.lista.get_selection().get_selected()
        if idx==None:return
        if it[idx][3]=='':return #Categoria
        url = it[idx][1]
        try:
            browser.Mozilla('chrome').open_new_tab(url)
        except:
            browser.Mozilla('firefox').open_new_tab(url)
        

    def exportar(self,evt):
        dlg = FileChooserDialog(parent=self.top,title="Exportar Favoritos",action=FILE_CHOOSER_ACTION_SAVE,buttons=('gtk-ok',1,'gtk-cancel',0))
        f1 = FileFilter()
        f1.set_name("JSON Export")
        f1.add_pattern("*.json")
        f2 = FileFilter()
        f2.set_name("Todos os arquivos")
        f2.add_pattern("*")
        dlg.add_filter(f1)
        dlg.add_filter(f2)
        if dlg.run():
            saida = {}
            for fav in self.db.execute("SELECT categoria,descricao,url FROM favoritos ORDER BY categoria,descricao").fetchall():
                if not(saida.has_key(fav[0])):saida[fav[0]]=[]
                saida[fav[0]].append([fav[1],fav[2]])
            saida=JSONEncoder().encode(saida)
            try:
                open(dlg.get_filename(),'w').write(saida)
            except Exception,e:print(e)
        dlg.destroy()

    def importar(self,evt):
        dlg = FileChooserDialog(parent=self.top,title="Importar Favoritos",action=FILE_CHOOSER_ACTION_OPEN,buttons=('gtk-ok',1,'gtk-cancel',0))
        f1 = FileFilter()
        f1.set_name("JSON Export")
        f1.add_pattern("*.json")
        f2 = FileFilter()
        f2.set_name("Todos os arquivos")
        f2.add_pattern("*")
        dlg.add_filter(f1)
        dlg.add_filter(f2)
        if dlg.run():
            try:
                entrada = JSONDecoder().decode(open(dlg.get_filename()).read())
                for k in entrada:
                    for fav in entrada[k]:
                        categoria=k
                        descricao=fav[0]
                        url = fav[1]
                        if self.db.execute("SELECT count(descricao) FROM favoritos WHERE url=?",(url,)).fetchone()[0]<=0:
                            self.db.execute("INSERT INTO favoritos VALUES(?,?,?)",(k,descricao,url))
                        else:
                            print("%s - %s -%s já existe no banco de dados"%(k,descricao,url))
                self.db.commit()
                self.atualizar()
            except Exception,e:print(e)
        dlg.destroy()

class DLGFav(Dialog):
    def __init__(self,parent):
        Dialog.__init__(self,"Favoritos",parent)
        frs = [Frame("Categoria:"),Frame("Descrição:"),Frame("URL:")]
        for fr in frs:self.vbox.pack_start(fr)
        self.categoria = Entry()
        self.descricao = Entry()
        self.url=Entry()
        frs[0].add(self.categoria)
        frs[1].add(self.descricao)
        frs[2].add(self.url)
        self.add_button('gtk-ok',1)
        self.add_button('gtk-cancel',0)
        self.show_all()

if __name__=="__main__":
    import sqlite3,os
    jn=Window()
    pt = os.path.join(os.environ.get("HOME"),".agenda/agenda.db")
    jn.db = sqlite3.dbapi2.connect(pt)
    jn.connect('destroy',main_quit)
    fav= Favoritos(jn)
    jn.add(fav)
    jn.set_size_request(640,480)
    jn.show_all()
    main()
   
    

