#
#-*-coding:utf-8-*-
#
from gtk import *
import aes
from hashlib import md5

class Notas(Frame):
    def __init__(self,parent):
        self.db=parent.db
        self.top=parent
        Frame.__init__(self)
        bx = VBox(False,0)
        tb = Toolbar()
        bt1 = ToolButton("gtk-new")
        bt1.connect("clicked",self.novo)
        bt2 = ToolButton("gtk-delete")
        bt2.connect("clicked",self.apagar)
        bt3 = ToolButton('gtk-refresh')
        bt3.connect("clicked",self.atualiza_internet)
        bt4 = ToolButton('gtk-go-up')
        bt4.set_label("Enviar p/ internet")
        bt4.connect("clicked",self.envia_internet)
        bt5 = ToolButton('gtk-go-down')
        bt5.set_label("Baixar da internet")
        bt5.connect("clicked",self.recebe_internet)        
        tb.add(bt1)
        tb.add(bt2)
        tb.add(SeparatorToolItem())
        tb.add(bt3)
        tb.add(SeparatorToolItem())
        tb.add(bt4)
        tb.add(bt5)
        bx.pack_start(tb,False,True)
        self.lista=TreeView()
        scw = ScrolledWindow()
        scw.set_policy(POLICY_AUTOMATIC,POLICY_AUTOMATIC)
        scw.add(self.lista)
        bx.pack_start(scw,True,True)
        
        self.add(bx)
        col=TreeViewColumn("",Renderer(),text=0,foreground=2)
        col.set_visible(False)
        self.lista.append_column(col)
        self.lista.append_column(TreeViewColumn("",Renderer(),text=1,foreground=2))
        self.lista.set_model(ListStore(str,str))
        self.lista.set_headers_visible(False)
        self.lista.connect("row-activated",self.abrir)
        self.atualizar()

    def novo(self,evt):
        dlg=DLGNota(self.top)
        if dlg.run():
            if self.db.execute("SELECT count(titulo) FROM notas WHERE titulo=?",(dlg.titulo.get_text(),)).fetchone()[0]<=0:
                self.db.execute("INSERT INTO notas(titulo,nota) VALUES(?,?)",(dlg.titulo.get_text(),dlg.get_nota()))
                self.db.commit()
                self.atualizar()
            else:
                dlgw = MessageDialog(self.top,DIALOG_MODAL,MESSAGE_ERROR,BUTTONS_OK,"Já existe mensagem com este titulo!")
                dlgw.run();dlgw.destroy()
        dlg.destroy()
    
    def abrir(self,evt,wdg,idx):
        md,it = self.lista.get_selection().get_selected()
        if it==None:return
        id=md.get_value(it,0)
        titulo = md.get_value(it,1)        
        cor = md.get_value(it,1)#Azul(#0000FF) -> Local | Verde(#00FF00) ->Internet
        if cor=='#00FF00':
            return(self.abre_internet(titulo))
        nota = self.db.execute("SELECT nota FROM notas WHERE rowid=?",(id,)).fetchone()[0]
        dlg=DLGNota(self.top)
        dlg.texto.get_buffer().set_text(nota)
        dlg.titulo.set_text(titulo)
        if dlg.run():
            self.db.execute("UPDATE notas SET nota=?,titulo=? WHERE rowid=?",(dlg.get_nota(),dlg.titulo.get_text(),id))
            self.db.commit()
            self.atualizar()
        dlg.destroy()

    def apagar(self,evt):
        md,it = self.lista.get_selection().get_selected()
        if it==None:return
        id = md.get_value(it,0)
        titulo = md.get_value(it,1)
        cor = md.get_value(it,2)
        dlg=MessageDialog(self.top,DIALOG_MODAL,MESSAGE_INFO,BUTTONS_YES_NO,"Apagar a nota %s"%titulo)
        if dlg.run()==RESPONSE_YES:        
            if cor=='#00FF00':
                dados={'usuario':self.db.execute("SELECT valor FROM config WHERE chave='usuario'").fetchone()[0],
                  'senha':self.db.execute("SELECT valor FROM config WHERE chave='senha'").fetchone()[0],'descricao':titulo}
                self.top.web.apaga_nota(dados)
                self.atualiza_internet(evt)
            else:
                self.db.execute("DELETE FROM notas WHERE rowid=?",(id,))
                self.db.commit()
                self.atualizar()
        dlg.destroy()

    def atualizar(self):
        md = ListStore(str,str,str)
        for i in self.db.execute("SELECT rowid,titulo,'#0000FF' FROM notas ORDER BY titulo").fetchall():md.append(i)
        self.lista.set_model(md)        

    def envia_internet(self,evt):
        md,it = self.lista.get_selection().get_selected()
        if it==None:return
        titulo = md.get_value(it,0)
        cor = md.get_value(it,1)
        if cor=='#00FF00':return # ja esta na internet
        nota = self.db.execute("SELECT nota FROM notas WHERE titulo=?",(titulo,)).fetchone()[0]
        dados={'usuario':self.db.execute("SELECT valor FROM config WHERE chave='usuario'").fetchone()[0],
              'senha':self.db.execute("SELECT valor FROM config WHERE chave='senha'").fetchone()[0],'descricao':titulo,'nota':nota}
        self.top.web.salvar_nota(dados)
        self.atualiza_internet(evt)        
        
    def recebe_internet(self,evt):
        md,it = self.lista.get_selection().get_selected()
        if it==None:return
        titulo = md.get_value(it,0)        
        cor = md.get_value(it,1)#Azul(#0000FF) -> Local | Verde(#00FF00) ->Internet
        if cor=='#0000FF':return    
        auth={'usuario':self.db.execute("SELECT valor FROM config WHERE chave='usuario'").fetchone()[0],
              'senha':self.db.execute("SELECT valor FROM config WHERE chave='senha'").fetchone()[0]}
        nota=self.top.web.abre_nota(auth,titulo)        
        count=self.db.execute("SELECT count(titulo) FROM notas WHERE titulo=?",(nota[1],)).fetchone()[0]
        if count<=0:
            self.db.execute("INSERT INTO notas VALUES(?,?)",(nota[1],nota[2]))
        else:
            self.db.execute("UPDATE notas SET nota=? WHERE titulo=?",(nota[1],nota[2]))
        self.db.commit()
        self.atualiza_internet(evt)


    def atualiza_internet(self,evt):
        self.atualizar()#remove todas as entradas e insere as locais
        md = self.lista.get_model()#obtem o modelo da lista de notas
        auth={'usuario':self.db.execute("SELECT valor FROM config WHERE chave='usuario'").fetchone()[0],
              'senha':self.db.execute("SELECT valor FROM config WHERE chave='senha'").fetchone()[0]}
        notasw=self.top.web.listar_notas(auth)
        for nota in notasw.keys():            
            md.append([notasw[nota],"#00FF00"])

    def abre_internet(self,titulo):
        auth={'usuario':self.db.execute("SELECT valor FROM config WHERE chave='usuario'").fetchone()[0],
              'senha':self.db.execute("SELECT valor FROM config WHERE chave='senha'").fetchone()[0]}
        nota=self.top.web.abre_nota(auth,titulo)        
        dlg=DLGNota(self.top)
        if nota[2]!=None:#Falha na leitura
            dlg.texto.get_buffer().set_text(nota[2])
        dlg.titulo.set_text(nota[1])
        if dlg.run():
            dados={'usuario':self.db.execute("SELECT valor FROM config WHERE chave='usuario'").fetchone()[0],
              'senha':self.db.execute("SELECT valor FROM config WHERE chave='senha'").fetchone()[0],
              'descricao':dlg.titulo.get_text(),
              'descricao_original':nota[1],
              'nota':dlg.get_nota()}
            self.top.web.salvar_nota(dados)
        dlg.destroy()

                        
        
class DLGNota(Dialog):
    def __init__(self,parent):
        Dialog.__init__(self,title="Nota",parent=parent,buttons=("gtk-ok",1,"gtk-cancel",0))
        fr = Frame("Titulo:")
        tb=Toolbar()
        imgs = [Image(),Image()]   
        imgs[0].set_from_pixbuf(gdk.pixbuf_new_from_xpm_data(lock_c))
        imgs[1].set_from_pixbuf(gdk.pixbuf_new_from_xpm_data(lock_o))
        bt1 =ToolButton(imgs[0])
        bt2 =ToolButton(imgs[1])        
        bt1.set_label("Criptografar")
        bt2.set_label("Decriptografar")
        bt1.connect("clicked",lambda evt:self.criptografar())
        bt2.connect("clicked",lambda evt:self.decriptografar())
        tb.add(bt1)
        tb.add(bt2)
        self.vbox.pack_start(tb,False,True)
        self.titulo=Entry()
        fr.add(self.titulo)
        fr2=Frame()
        self.texto = TextView()
        scw = ScrolledWindow()
        scw.set_policy(POLICY_AUTOMATIC,POLICY_AUTOMATIC)
        scw.add(self.texto)
        fr2.add(scw)
        self.texto.set_size_request(400,400)
        self.vbox.pack_start(fr,False,True)
        self.vbox.pack_end(fr2,True,True)
        self.show_all()

    def get_nota(self):
        bf = self.texto.get_buffer()
        fit = bf.get_start_iter()
        eit = bf.get_end_iter()
        return(bf.get_text(fit,eit))

    def criptografar(self):
        dlg=Dialog()
        dlg.set_title("Senha")
        dlg.add_buttons('gtk-ok',1,'gtk-cancel',0)
        senha=Entry()
        fr=Frame("Senha:")
        senha.set_visibility(False)
        fr.add(senha)
        dlg.vbox.pack_start(fr,True,True)
        dlg.show_all()
        if dlg.run():
            try:
                senha = md5(senha.get_text()).hexdigest()
                self.texto.get_buffer().set_text(aes.encryptData(senha,self.get_text()).encode("base64"))        
            except Exception,e:
                d=MessageDialog(parent=self,buttons=BUTTONS_OK,type=MESSAGE_ERROR)
                d.set_markup(str(e))
                d.run();d.destroy()
        dlg.destroy()

    def decriptografar(self):
        dlg=Dialog()
        dlg.set_title("Senha")
        dlg.add_buttons('gtk-ok',1,'gtk-cancel',0)
        senha=Entry()
        senha.set_visibility(False)
        fr=Frame("Senha:")
        fr.add(senha)
        dlg.vbox.pack_start(fr,True,True)
        dlg.show_all()
        if dlg.run():
            try:
                senha = md5(senha.get_text()).hexdigest()
                self.texto.get_buffer().set_text(aes.decryptData(senha,self.get_text().decode("base64")))
            except Exception,e:
                d=MessageDialog(parent=self,buttons=BUTTONS_OK,type=MESSAGE_ERROR)
                d.set_markup(str(e))
                d.run();d.destroy()
        dlg.destroy()

    def get_text(self):
        bf = self.texto.get_buffer()
        ini =bf.get_start_iter()
        end = bf.get_end_iter()
        return(bf.get_text(ini,end))
              
        
class Renderer(CellRendererText):
    def __init__(self):
        CellRendererText.__init__(self)
        self.set_property('foreground-set',True)        

lock_c=["22 22 97 2",
"  	c None",
". 	c #B4B4B4",
"+ 	c #B5B5B5",
"@ 	c #BEBEBE",
"# 	c #C3C3C3",
"$ 	c #BABABA",
"% 	c #BCBCBC",
"& 	c #BFBFBF",
"* 	c #B9B9B9",
"= 	c #CBCBCB",
"- 	c #CECECE",
"; 	c #C1C1C1",
"> 	c #CCCCCC",
", 	c #BBBBBB",
"' 	c #D7D7D7",
") 	c #CFCFCF",
"! 	c #C7C7C7",
"~ 	c #CDCDCD",
"{ 	c #B6B6B6",
"] 	c #D5D5D5",
"^ 	c #E3E3E3",
"/ 	c #ABABAB",
"( 	c #AAAAAA",
"_ 	c #C6C6C6",
": 	c #B3B3B3",
"< 	c #CACACA",
"[ 	c #B2B2B2",
"} 	c #AFAFAF",
"| 	c #DADADA",
"1 	c #EAEAEA",
"2 	c #ACACAC",
"3 	c #D2D2D2",
"4 	c #DCDCDC",
"5 	c #DEDEDE",
"6 	c #9E9E9E",
"7 	c #BDBDBD",
"8 	c #C0C0C0",
"9 	c #ADADAD",
"0 	c #B8B8B8",
"a 	c #C4C4C4",
"b 	c #AEAEAE",
"c 	c #D4D4D4",
"d 	c #969696",
"e 	c #A3A3A3",
"f 	c #9D9D9D",
"g 	c #989898",
"h 	c #A0A0A0",
"i 	c #C5C5C5",
"j 	c #9C9C9C",
"k 	c #B1B1B1",
"l 	c #959595",
"m 	c #A5A5A5",
"n 	c #C8C8C8",
"o 	c #D1D1D1",
"p 	c #9F9F9F",
"q 	c #C2C2C2",
"r 	c #D3D3D3",
"s 	c #A1A1A1",
"t 	c #A2A2A2",
"u 	c #E5E5E5",
"v 	c #B0B0B0",
"w 	c #C9C9C9",
"x 	c #E6E6E6",
"y 	c #A6A6A6",
"z 	c #939393",
"A 	c #DBDBDB",
"B 	c #919191",
"C 	c #A4A4A4",
"D 	c #999999",
"E 	c #E1E1E1",
"F 	c #838383",
"G 	c #8A8A8A",
"H 	c #9B9B9B",
"I 	c #949494",
"J 	c #767676",
"K 	c #D0D0D0",
"L 	c #868686",
"M 	c #828282",
"N 	c #8C8C8C",
"O 	c #878787",
"P 	c #8F8F8F",
"Q 	c #757575",
"R 	c #888888",
"S 	c #7C7C7C",
"T 	c #787878",
"U 	c #7E7E7E",
"V 	c #848484",
"W 	c #8E8E8E",
"X 	c #979797",
"Y 	c #8B8B8B",
"Z 	c #7B7B7B",
"` 	c #727272",
" .	c #797979",
"..	c #A8A8A8",
"+.	c #909090",
"@.	c #A7A7A7",
"#.	c #9A9A9A",
"                                            ",
"              . + @ # $ @ % $ $ @           ",
"          & * @ = - $ ; > & , $ - '         ",
"        - & $ )       =     ! ~ { , ]       ",
"      ^ / ( =                   _ : <       ",
"      > [ } | 1                   2 3 4     ",
"    5 6 7 2 , 7 8 + 2 / 9 % < 0 a b c #     ",
"      d % e f g h [ . 8 i 7 ; } j b a 2     ",
"      k / l m ! > - # 0 n ) o 3 & p : m     ",
"      ( j * ~ _ * b 7 < % { 0 q r q s t     ",
"    u 6 v w ~ a @ # c x 5 ] n & w - y p c   ",
"    & z < w + & ~ o ] A     r 0 k ~ , h >   ",
"    y B A 0 z v w < q = A c i } f ; r C .   ",
"    D z E k F 6 b $ @ & i , + / j 0 5 t D _ ",
"  4 B G 5 % G B B D t t s h s 6 s + c H G ! ",
"    I J K ! l L M F N N N D d O f : a g P   ",
"    C Q } & s R S T U V R z W L j 9 } d y   ",
"    - X Y C { ( z Z `  .U F P p ..9 D p )   ",
"      ! l +.} & : @.s 6 e 9 : v / @.h n     ",
"        ; f l s / [ { , # a . p D s <       ",
"          ~ } 6 t h #.6 e e H 6 % |         ",
"              = v h D z d C 0 >             "]

lock_o=["22 22 91 1",
" 	c None",
".	c #B6B6B6",
"+	c #C0C0C0",
"@	c #C5C5C5",
"#	c #BDBDBD",
"$	c #BEBEBE",
"%	c #C3C3C3",
"&	c #BBBBBB",
"*	c #B8B8B8",
"=	c #D3D3D3",
"-	c #CCCCCC",
";	c #BABABA",
">	c #C4C4C4",
",	c #D0D0D0",
"'	c #CECECE",
")	c #C6C6C6",
"!	c #C9C9C9",
"~	c #CBCBCB",
"{	c #D4D4D4",
"]	c #C2C2C2",
"^	c #CDCDCD",
"/	c #BFBFBF",
"(	c #B7B7B7",
"_	c #D2D2D2",
":	c #DDDDDD",
"<	c #BCBCBC",
"[	c #B3B3B3",
"}	c #B2B2B2",
"|	c #ADADAD",
"1	c #B0B0B0",
"2	c #ACACAC",
"3	c #B9B9B9",
"4	c #AFAFAF",
"5	c #B1B1B1",
"6	c #AEAEAE",
"7	c #C8C8C8",
"8	c #A3A3A3",
"9	c #A1A1A1",
"0	c #A7A7A7",
"a	c #A9A9A9",
"b	c #9F9F9F",
"c	c #C7C7C7",
"d	c #CACACA",
"e	c #C1C1C1",
"f	c #CFCFCF",
"g	c #A8A8A8",
"h	c #A6A6A6",
"i	c #A0A0A0",
"j	c #DADADA",
"k	c #E8E8E8",
"l	c #DFDFDF",
"m	c #9D9D9D",
"n	c #D6D6D6",
"o	c #D7D7D7",
"p	c #ABABAB",
"q	c #AAAAAA",
"r	c #909090",
"s	c #D1D1D1",
"t	c #B4B4B4",
"u	c #B5B5B5",
"v	c #8A8A8A",
"w	c #8F8F8F",
"x	c #929292",
"y	c #9C9C9C",
"z	c #989898",
"A	c #7D7D7D",
"B	c #9E9E9E",
"C	c #888888",
"D	c #858585",
"E	c #898989",
"F	c #919191",
"G	c #939393",
"H	c #9A9A9A",
"I	c #969696",
"J	c #9B9B9B",
"K	c #8C8C8C",
"L	c #828282",
"M	c #7B7B7B",
"N	c #797979",
"O	c #818181",
"P	c #868686",
"Q	c #8D8D8D",
"R	c #878787",
"S	c #7A7A7A",
"T	c #737373",
"U	c #979797",
"V	c #A5A5A5",
"W	c #D5D5D5",
"X	c #A4A4A4",
"Y	c #A2A2A2",
"Z	c #999999",
"       .+@#$%&*%=     ",
"     -;>,'$)!$+%~{    ",
"    {#]^       /(-    ",
"   _~/@         (!:   ",
"   <;[[               ",
"   }}|                ",
"   1|2                ",
"   [34  @<56(7)       ",
"   1}2890[[&/+&02     ",
"   53ab$cde#d^f%g2    ",
"   694d731+d&(#'chi   ",
"   *a%d@ecjkl=%]~|h=  ",
"   m(^4.^,,n oepeeq@  ",
"   r]cri<>eds@tbusqg  ",
"  ;vedwxy066|ahi[fbzf ",
"  pA5_BCDExFGHFI}]JK  ",
"   Lz%gCMNOPQGRx21Hb  ",
"   5Qm}qxSTMOEUVpyh~  ",
"   W|rX35Vbi044g8g>   ",
"    c5BBa64u<uYi1@    ",
"      $60iHyYhq._     ",
"       ,}bUxZ|@       "]

