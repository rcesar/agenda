#
#-*-coding:utf-8-*-
#
import urllib2
import urllib 
import json

class Webservices:
    def __init__(self,parent):
        self.db=parent.db
        self.url = "http://agendarc.zzl.org"
        self.parent=parent
        
    def ler(self,url,dd=None):        
        op=self.get_opener()
        dados=''
        try:           
            dados=op(url).read()
            dados=json.loads(dados)            
        except Exception,e:
            self.parent.alert(str(e)+"\n"+dados,'ERROR')        
            return({})
        return(dados)
        
    def listar_pendencias(self,dados):
        url=self.url+"/pendencias.php"
        dados=urllib.urlencode(dados)
        url=urllib2.Request(url,dados)
        op=self.get_opener()
        try:
            dados=op(url).read()
            return(json.loads(dados))
        except Exception,e:
            self.parent.alert(str(e)+"\n"+dados,'ERROR')        
            return([])

    def abrir_pendencia(self,dados):
        url=self.url+"/pendencias.php?op=abrir"
        dados=urllib.urlencode(dados)
        url=urllib2.Request(url,dados)
        op=self.get_opener()
        try:
            dados=op(url).read()          
            return(json.loads(dados))
        except Exception,e:
            self.parent.alert(str(e),'ERROR')        
            return['','']

    def salvar_pendencia(self,dados):
        url=self.url+"/pendencias.php?op=editar"
        dados=urllib.urlencode(dados)
        url=urllib2.Request(url,dados)
        op=self.get_opener()
        try:
            dados=op(url).read()          
            self.parent.alert(dados)
        except Exception,e:
            self.parent.alert(str(e),'ERROR')        
            return['','']


    def apagar_pendencia(self,dados):
        url=self.url+"/pendencias.php?op=apagar"
        dados=urllib.urlencode(dados)
        url=urllib2.Request(url,dados)
        op=self.get_opener()
        try:
            dados=op(url).read()          
            self.parent.alert(dados)
        except Exception,e:
            self.parent.alert(str(e),'ERROR')        
            return['','']


    def listar_compromissos(self,dados):
        url=self.url+"/compromisso_listar.php"
        dados=urllib.urlencode(dados)
        url=urllib2.Request(url,dados)
        op=self.get_opener()
        try:
            dados=op(url).read()
            return(json.loads(dados))
        except Exception,e:        
            self.parent.alert(str(e)+"\n"+dados,'ERROR')        
            return([])

    def  abrir_compromisso(self,dados):
        url=self.url+"/Compromisso.php"
        dados=urllib.urlencode(dados)
        url=urllib2.Request(url,dados)
        op=self.get_opener()
        try:
            dados=op(url).read()          
            return(json.loads(dados))
        except Exception,e:
            self.parent.alert(str(e),'ERROR')        
            return['','','']

    def salvar_compromisso(self,dados):
        url=self.url+"/compromisso_salvar.php"
        dados=urllib.urlencode(dados)
        url=urllib2.Request(url,dados)
        op=self.get_opener()
        try:
            dados=op(url).read()
            self.parent.alert(dados)
        except Exception,e:
            self.parent.alert(str(e),'ERROR')        

    def apagar_compromisso(self,dados):
        url=self.url+"/compromisso_apagar.php"
        dados=urllib.urlencode(dados)
        url=urllib2.Request(url,dados)
        op=self.get_opener()
        try:
            dados=op(url).read()
            self.parent.alert(dados)
        except Exception,e:
            self.parent.alert(str(e),'ERROR')        
                

    def listar_notas(self,auth):
        url = self.url+"/listarnotas.php?usuario="+auth['usuario']+"&senha="+auth['senha']
        return(self.ler(url))
        
    def abre_nota(self,auth,titulo):
        auth['descricao']=titulo
        url = self.url+"/nota.php?"+urllib.urlencode(auth)        
        ret=self.ler(url)
        ret[2]=ret[2].decode('base64')
        return(ret)

    def apaga_nota(self,dados):
        url=self.url+'/apagarnota.php'
        dados=urllib.urlencode(dados)
        url=urllib2.Request(url,dados)
        op=self.get_opener()
        try:
            dados=op(url).read()
            self.parent.alert(dados)
        except Exception,e:
            self.parent.alert(str(e),'ERROR')        
        
    def salvar_nota(self,dados):
        url=self.url+'/salvarnota.php'
        dados=urllib.urlencode(dados)
        url=urllib2.Request(url,dados)
        op=self.get_opener()
        try:
            dados=op(url).read()
            self.parent.alert(dados)
        except Exception,e:
            self.parent.alert(str(e),'ERROR')        
       
        
    def get_opener(self):
        proxy= self.db.execute("SELECT valor FROM config WHERE chave='proxy'").fetchone()[0]
        usuario= self.db.execute("SELECT valor FROM config WHERE chave='proxy_user'").fetchone()[0]
        senha = self.db.execute("SELECT valor FROM config WHERE chave='proxy_password'").fetchone()[0]
        if proxy.lower()=='none':
            opener=urllib2.urlopen
        else:
            opener=self.pxhandler({'servidor':proxy,'usuario':usuario,'senha':senha})
        return(opener)

    def pxhandler(self,px):
        servidor='http://'+px['usuario']+":"+px['senha']+"@"+px['servidor']
        proxy=urllib2.ProxyHandler({'http':servidor})
        auth=urllib2.HTTPBasicAuthHandler()
        return(urllib2.build_opener(proxy,auth,urllib2.HTTPHandler).open)
    
