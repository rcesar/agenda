#
#-*-coding:utf-8-*-
#

from gtk import *

class Exportar(Window):
    def __init__(self,parent):
        self.db = parent.db
        self.top = parent
        Window.__init__(self)
        bx = VBox(False,False)
        self.add(bx)
        self.set_title("Exportar/Importar dados")        
#        campos = {}
#        for tabela in self.db.execute("SELECT name,sql FROM SQLITE_MASTER WHERE type='table'").fetchall():
#            campos[tabela[0]]=[]
#            for campo in tabela[1][tabela[1].find("("):].replace(")","").split(","):
#                nome = campo.split(" ")[0]
#                campos[tabela[0]].append(nome)
        campos = {'compromissos':['data','descricao'],'pendencias':['descricao'],'notas':['descricao'],'favoritos':['categoria','descrcicao']}
        self.lista_tabelas = campos
        fr = Frame("Tabelas:")
        self.tabelas =combo_box_new_text()
        self.campos= TreeView()
        self.resultados = TreeView()
        self.resultados.set_size_request(400,300)
        tbx = HBox(False,False)
        fr.add(tbx)
        tbx.pack_start(self.tabelas,True,True)
        bt = ToolButton("gtk-refresh")
        bt.connect("clicked",lambda evt:self.abrir_tabela())
        tbx.pack_end(bt,False,False)
        for c in self.lista_tabelas.keys():self.tabelas.append_text(c)
        self.tabelas.set_active(0)
        bx.pack_start(fr,False,True)
        fr = Frame("Campos:")
        bxc = VBox(False,False)
        fr.add(bxc)
        bt1 = Button("Adicionar filtro")
        bt2 = Button("Remover filtro")
        bt3 = Button("Exportar")
        bt4 = Button("Importar")
        bt5 = Button("Fechar")
        scw = ScrolledWindow()
        scw.set_policy(POLICY_AUTOMATIC,POLICY_AUTOMATIC)
        scw.add(self.campos)
        bxc.pack_start(scw,True,True)
        bxc.pack_start(bt1,False,False)
        bxc.pack_start(bt2,False,False)
        bxc.pack_start(bt3,False,False)
        bxc.pack_start(bt4,False,False)
        bxc.pack_start(bt5,False,False)
        base = HBox(False,False)
        bx.pack_start(base,True,True)
        base.pack_start(fr,True,True)
        frt = Frame()
        scw = ScrolledWindow()
        scw.set_policy(POLICY_AUTOMATIC,POLICY_AUTOMATIC)
        scw.add(self.resultados)
        frt.add(scw)
        base.pack_start(frt,True,True)

    def abrir_tabela(self):pass


if __name__=="__main__":
        import sqlite3.dbapi2 as sqlite
        class tste:pass
        teste = tste()
        teste.db = sqlite.connect("db/agenda.db")
        jn=Exportar(teste)
        jn.connect("destroy",main_quit)
        jn.show_all()
        main()

        
