#!
#-*-coding:utf-8-*-

from gtk import *
from DatePicker import DatePicker
from sqlite3 import dbapi2 as sqlite
import os,sys,time

class Adiant(Frame):
    def __init__(self,top):
	self.menubar = top.menu
	self.db=top.db
	Frame.__init__(self)
        base = VBox(False,0)
        base.pack_start(self.make_toolbar(),False,True)
        scw = ScrolledWindow()
        self.lista= TreeView()
        scw.add(self.lista)
        scw.set_policy(POLICY_AUTOMATIC,POLICY_AUTOMATIC)
        colunas = [TreeViewColumn("Código",CellRendererText(),text=0),
                           TreeViewColumn("Nome"+(30*" "),CellRendererText(),text=1),
                           TreeViewColumn("Adiant mês",CellRendererText(),text=2),
                           TreeViewColumn("valor",CellRendererText(),text=3)]
        #colunas[1].set_attributes("width",size=300)
        for coluna in colunas:self.lista.append_column(coluna)
        self.lista.set_grid_lines(True)
        self.lista.set_search_column(1)
        base.pack_end(scw,True,True)
        self.add(base)
        self.atualizar()

    def make_toolbar(self):
        tb =Toolbar()
        #tb.add(SeparatorToolItem())
        bt2 = ToolButton("gtk-add");bt2.connect("clicked",lambda evt=None:self.add_adiant())
        tb.add(bt2)
        bt3 = ToolButton("gtk-remove");bt3.connect("clicked",lambda evt=None:self.del_adiant())
        tb.add(bt3)
        return(tb)


    def add_adiant(self):
        md,iter = self.lista.get_selection().get_selected()
        if iter==None:return
        codigo = md.get_value(iter,0)
        nome = md.get_value(iter,1)
        dlg = DLGAd(self)
        dlg.codigo.set_text(codigo)
        dlg.nome.set_text(nome)
        dlg.data.set(time.strftime("%Y-%m-%d"))
        if dlg.run():
            contador = self.db.execute("SELECT count(data) FROM adiantamento WHERE data=? AND cliente=?",(dlg.codigo.get_text(),dlg.data.get())).fetchone()[0]
            if contador<=0:
                self.db.execute("INSERT INTO adiantamento VALUES(?,?,?)",(dlg.data.get(),dlg.codigo.get_text(),dlg.valor.get_text()))
                self.db.commit()
                self.atualizar()
        dlg.destroy()

    def del_adiant(self):
        md,iter = self.lista.get_selection().get_selected()
        if iter==None:return
        codigo = md.get_value(iter,0)
        nome = md.get_value(iter,1)
        dlg = DLGListaAd()
        md = ListStore(str,str)
        for item in self.db.execute("SELECT data,valor FROM adiantamento WHERE cliente=?",(codigo,)).fetchall():md.append(item)
        dlg.lista.set_model(md)
        dlg.add_button("gtk-ok",1);dlg.add_button("gtk-cancel",0)
        if dlg.run():
            md,iter = dlg.lista.get_selection().get_selected()
            if iter==None:return
            data=md.get_value(iter,0)
            self.db.execute("DELETE FROM adiantamento WHERE cliente=? AND data=?",(codigo,data))
            self.db.commit()
            self.atualizar()
        dlg.destroy()

    def add_cliente(self):
       dlg = DLGCliente(self)
       if dlg.run():
           contador = self.db.execute("SELECT count(mci) FROM carteira WHERE mci=?",dlg.codigo.get_text()).fetchone()[0]
           if contador<=0:
               self.db.execute("INSERT INTO carteira(mci,nome) VALUES(?,?)",(dlg.codigo.get_text(),dlg.nome.get_text()))
           else:
               self.db.execute("UPDATE carteira SET nome=? WHERE mci=?",(dlg.nome.get_text(),dlg.codigo.get_text()))
           self.db.commit()
           self.atualizar()
       dlg.destroy()

    def del_cliente(self):
        md,iter = self.lista.get_selection().get_selected()
        if iter==None:return
        codigo = md.get_value(iter,0)
        nome = md.get_value(iter,1)
        message= "Apagar os dados do cliente:\n%s\n%s"%(codigo,nome)
        dlg=MessageDialog(self,DIALOG_MODAL,MESSAGE_INFO,BUTTONS_YES_NO,message)
        if dlg.run()==RESPONSE_YES:
            self.db.execute("DELETE FROM carteira WHERE mci=?",(codigo,))
            self.db.execute("DELETE FROM adiantamento WHERE cliente=?",(codigo,))
            self.db.commit()
            self.atualizar()
        dlg.destroy()

    def atualizar(self):
        sql_adiant = "SELECT count(data),SUM(valor) FROM adiantamento WHERE cliente=? AND data>=? AND data<=?"
        data_ini=time.strftime("%Y-%m-01")
        if (int(time.strftime("%m")) in [1,3,5,7,8,10,12]):
            data_fim=time.strftime("%Y-%m-31")
        else:data_fim=time.strftime("%Y-%m-30")
        if int(time.strftime("%m"))==2:
            if bisexto():
                data_fim=time.strftime("%Y-%m-28")
            else:data_fim=time.strftime("%Y-%m-28")
        md = ListStore(str,str,str,str)
        for cliente in self.db.execute("SELECT mci,nome FROm carteira ORDER BY nome").fetchall():
            adiant,valor = self.db.execute(sql_adiant,(cliente[0],data_ini,data_fim)).fetchone()
            if not(valor):valor=0
            linha  = [cliente[0],cliente[1],"%.2i"%adiant,"%.2f"%valor]
            md.append(linha)
        self.lista.set_model(md)



#--------------------------------------------------------------------------
class DLGAd(Dialog):
        def __init__(self,top):
                Dialog.__init__(self)
                self.set_title("Adiantamento a depositantes")
                self.codigo = Entry();self.empacotar(self.codigo,"Código")
                self.codigo.set_property("editable",False)
                self.nome= Entry();self.empacotar(self.nome,"Nome:");
                self.nome.set_property("editable",False)
                self.data = DatePicker("Data:");self.vbox.pack_start(self.data,False,True)
                self.valor=Entry();self.empacotar(self.valor,"Valor:")
                self.add_button("gtk-ok",1)
                self.add_button("gtk-cancel",0)
                self.show_all()

        def empacotar(self,wdg,label=""):
                fr = Frame(label)
                fr.add(wdg)
                self.vbox.pack_start(fr,False,True)

class DLGCliente(Dialog):
    def __init__(self,top):
        Dialog.__init__(self)
        self.codigo = Entry();self.empacotar(self.codigo,"Código:")
        self.nome = Entry();self.empacotar(self.nome,"Nome:")
        self.add_button("gtk-ok",1)
        self.add_button("gtk-cancel",0)
        self.show_all()

    def empacotar(self,wdg,label=""):
                fr = Frame(label)
                fr.add(wdg)
                self.vbox.pack_start(fr,False,True)

class DLGListaAd(Dialog):
    def __init__(self):
        Dialog.__init__(self)
        self.lista = TreeView()
        self.lista.append_column(TreeViewColumn("Data:",CellRendererText(),text=0))
        self.lista.append_column(TreeViewColumn("Valor:",CellRendererText(),text=1))
        self.vbox.add(self.lista)
        self.show_all()
#---------------------------------------------------------------------------
def bisexto():
    ano=int(time.strftime("%Y"))
    if ano % 4  != 0  :return(False)
    if ano % 100 != 0 :return(True)
    if ano % 400 != 0 :return(False)
    return(True)

def input_dialog(label=''):
    dlg=Dialog()
    dlg.set_title("Adiantamento a depositantes")
    fr = Frame(label)
    dlg.vbox.add(fr)
    vl = Entry()
    fr.add(vl)
    dlg.add_button('gtk-ok',1)
    dlg.add_button('gtk-cancel',0)
    dlg.show_all()
    if dlg.run():
        ret=vl.get_text()
    else:
        ret=''
    dlg.destroy()
    return(ret)
#---------------------------------------------------------------------------
if __name__=="__main__":pass
#    Adiant().show_all()
#    main()

