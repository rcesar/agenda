#
#-*-coding:utf-8-*-
#
from gtk import *
from Notas import Renderer
from sqlite3 import dbapi2 as sqlite

class Alarme:
    def __init__(self,parent):
        self.top=parent
        self.lista = sqlite.connect(":memory:")
        self.lista.execute("CREATE TABLE horarios(horario CHAR(5))")
        for horario in self.top.db.execute("SELECT hora FROM alarme").fetchall():self.lista.execute("INSERT INTO horarios VALUES(?)",horario)

    def check(self,horario):
        if self.lista.execute("SELECT count(horario) FROM horarios WHERE horario=?",(horario,)).fetchone()[0]>0:
            self.lista.execute("DELETE FROM horarios WHERE horario=?",(horario,))
            self.top.alert("DESPERTADOR \n%s"%horario)

    def show(self):
        if not(hasattr(self,'jn')) or self.jn==None:
            self.make_window()
        else:
            self.jn.show()

    def make_window(self):
        self.jn=Window()
        self.jn.set_title("Horarios de alarme")
        lista=TreeView()
        lista.append_column(TreeViewColumn("Horario",Renderer(),text=0,background=1))
        lista.set_headers_visible(False)
        base = VBox(False,False)
        tb = Toolbar()
        bt1 = ToolButton("gtk-close");bt1.connect("clicked",lambda evt:self.jn.destroy())
        tb.add(bt1)
        tb.add(SeparatorToolItem())
        bt1 = ToolButton("gtk-new");bt1.connect("clicked",self.add)
        tb.add(bt1)
        bt1 = ToolButton("gtk-delete");bt1.connect("clicked",self.apagar)
        tb.add(bt1)
        base.pack_start(tb,False,True)
        base.pack_start(lista,True,True)
        setattr(self.jn,'lista',lista)
        self.jn.add(base)
        self.jn.set_size_request(400,400)
        self.jn.connect("destroy",lambda evt:setattr(self,'jn',None))
        self.atualizar()
        self.jn.show_all()

    def atualizar(self):
        md = ListStore(str,str)
        for item in self.top.db.execute("SELECT hora FROM alarme").fetchall():
            md.append([item[0],"#FF0000"])
        for item in self.lista.execute("SELECT horario FROM horarios").fetchall():
            if self.top.db.execute("SELECT count(hora) FROM alarme WHERE hora=?",(item[0],)).fetchone()[0]<=0:
                md.append([item[0],"#00FF00"])
        self.jn.lista.set_model(md)        

    def add(self,evt):
        dlg=DLGAlarme(self.jn)
        if dlg.run():
            if dlg.permanente.get_active():
                self.top.db.execute("INSERT INTO alarme VALUES(?)",(dlg.horario.get_text(),))
                self.lista.execute("INSERT INTO horarios VALUES(?)",(dlg.horario.get_text(),))
                self.top.db.commit()
            else:
                self.lista.execute("INSERT INTO horarios VALUES(?)",(dlg.horario.get_text(),))
            self.atualizar()
        dlg.destroy()

    def apagar(self,evt):
        md,it = self.jn.lista.get_selection().get_selected()
        if it==None:return
        horario = md[it][0]
        tipo = md[it][1]
        if tipo=='#FF0000':
            self.top.db.execute("DELETE FROM alarme WHERE hora=?",(horario,))
            self.top.db.commit()
        self.lista.execute("DELETE FROM horarios WHERE horario=?",(horario,))
        self.atualizar()
        
        
class DLGAlarme(Dialog):
    def __init__(self,parent):
        Dialog.__init__(self,parent=parent)
        fr=Frame("Horario:")                
        self.horario=Entry()
        self.permanente=CheckButton("Permanente:")
        fr.add(self.horario)
        self.vbox.pack_start(fr)
        self.vbox.pack_start(self.permanente)
        self.add_buttons("gtk-ok",1,"gtk-cancel",0)
        self.show_all()


if __name__=="__main__":execfile('MainWindow.py',{"__name__":"__main__"})
