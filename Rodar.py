#
#-*-coding:utf-8-*-
#
from gtk import *
import glob,os,sys
import subprocess as sb

class Log(Frame):
	def __init__(self):
		Frame.__init__(self,"Log das execuções:")
		base=VBox(False,False)
		self.saida=TextView()
		self.saida.set_editable(False)
		self.erros=TextView()
		self.erros.set_editable(False)
		fr1=Frame("Saida:")
		fr2=Frame("Erros:")
		scw1=ScrolledWindow();scw1.set_policy(POLICY_AUTOMATIC,POLICY_AUTOMATIC)
		scw2=ScrolledWindow();scw2.set_policy(POLICY_AUTOMATIC,POLICY_AUTOMATIC)
		scw1.add(self.saida)
		scw2.add(self.erros)
		fr1.add(scw1)
		fr2.add(scw2)
		base.pack_start(fr1,True,True)
		base.pack_start(fr2,True,True)
		bt=Button("Limpar")
		bt.connect("clicked",lambda evt:self.limpar_log())
		base.pack_end(bt,False,True)
		self.add(base)

	def get_text(self):
		bf = self.saida.get_buffer()
		saida=bf.get_text(bf.get_start_iter(),bf.get_end_iter())
		bf = self.erros.get_buffer()
		erros=bf.get_text(bf.get_start_iter(),bf.get_end_iter())
		return(saida,erros)

	def log(self,pid):
		saida,erros=self.get_text()
		self.saida.get_buffer().set_text(saida+pid[0]+"\n")
		self.erros.get_buffer().set_text(erros+pid[1]+"\n")


	def limpar_log(self):
		self.saida.get_buffer().set_text("")
		self.erros.get_buffer().set_text("")
		

class Exec(Window):
	def __init__(self):
		Window.__init__(self)
		base=HBox(False,False)
		self.set_size_request(800,600)
		self.path=os.path.abspath(os.path.dirname(sys.argv[0]))
		self.set_title("Scripts python no diretorio:%s"%self.path)
		self.gerar_menu()
		scw=ScrolledWindow()
		self.lista=TreeView()
		self.lista.append_column(TreeViewColumn("",CellRendererText(),text=0))
		self.lista.connect("row-activated",self.executar)
		self.lista.connect("button_press_event",self.on_treeview_button_press_event)
		scw.add(self.lista)
		scw.set_policy(POLICY_AUTOMATIC,POLICY_NEVER)
		self.log=Log()
		base.pack_start(scw,True,True)
		base.pack_end(self.log,True,True)
		self.add(base)
		self.atualizar()
		
	def on_treeview_button_press_event(self, treeview, event):
	    if event.button == 3:
	       	x = int(event.x)
	        y = int(event.y)
	        time = event.time
	        pthinfo = treeview.get_path_at_pos(x, y)
	        if pthinfo is not None:
	       	    path, col, cellx, celly = pthinfo
	       	    treeview.grab_focus()
	       	    treeview.set_cursor( path, col, 0)
	       	    self.popup.popup( None, None, None, event.button, time)
		else:
	       	    self.popup2.popup( None, None, None, event.button, time)
	        return True

	def gerar_menu(self):
		menu=Menu()
		item11=MenuItem('Editar')
		item12=MenuItem('Executar script')	
		item13=MenuItem('Atualizar Listagem')
		item11.connect("activate",self.editar)
		item12.connect("activate",self.executar)
		item13.connect("activate",lambda *arhs:self.atualizar())
		menu.append(item11)
		menu.append(item12)
		menu.append(item13)
		menu2=Menu()
		item21=MenuItem('Atualizar listagem')
		item21.connect("activate",lambda *arhs:self.atualizar())
		menu2.append(item21)
		menu.show_all()
		menu2.show_all()
		self.popup=menu
		self.popup2=menu2
	
	def atualizar(self):
		md=ListStore(str)
		for nome in glob.glob(os.path.join(self.path,"*.py")):
			if nome!=os.path.abspath(sys.argv[0]):
				md.append([nome])
		self.lista.set_model(md)

	def executar(self,*args):
		md,idx=self.lista.get_selection().get_selected()
		if idx==None:return
		nome=md.get_value(idx,0)
		pid=sb.Popen([sys.executable,nome],stdout=sb.PIPE,stderr=sb.PIPE)
		retorno=pid.communicate()
		self.log.log(retorno)

	def editar(self,*args):
		md,idx=self.lista.get_selection().get_selected()
		if idx==None:return
		nome=md.get_value(idx,0)
		os.system("gedit %s &"%nome)

if __name__=="__main__":
	jn=Exec()
	jn.connect("destroy",main_quit)
	windowicon = jn.render_icon(STOCK_EXECUTE, ICON_SIZE_MENU)
	jn.set_icon(windowicon)
	jn.show_all()
	main()
