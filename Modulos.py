#
#-*-coding:utf-8-*-
#

import os,glob
from gtk import *

class Modulos(Frame):
    def __init__(self,parent):
        self.top=parent
        Frame.__init__(self)
        #self.path=os.path.join(os.path.abspath(os.path.dirname(sys.argv[0])),"Modulos")
        if os.name=='nt':
            self.path=os.path.join(os.environ.get("HOMEDRIVE"),os.environ.get("HOMEPATH"),".agenda","Modulos")
        else:
            self.path=os.path.join(os.environ.get("HOME"),".agenda","Modulos")
        if not(os.path.exists(self.path)):os.mkdir(self.path)
        bx=VBox(0,False)
        scw = ScrolledWindow()
        self.lista = TreeView()
        self.lista.connect("row-activated",self.rodar)
        self.lista.append_column(TreeViewColumn("",CellRendererText(),text=0))
        scw.add(self.lista)        
        scw.set_policy(POLICY_AUTOMATIC,POLICY_AUTOMATIC)
        bx.pack_start(scw)
        tb = Toolbar()
        bt = ToolButton("gtk-refresh")
        bt2 = ToolButton("gtk-open")
        bt3 = ToolButton("gtk-delete")
        bt2.set_label("Instalar novo")
        bt2.connect("clicked",lambda evt:self.instalar())
        bt.connect("clicked",lambda evt:self.atualizar())
        bt3.connect("clicked",lambda evt:self.remover())
        tb.add(bt);tb.add(bt2)
        tb.add(SeparatorToolItem());tb.add(bt3)
        bx.pack_end(tb,False,False)        
        self.add(bx)
        self.atualizar()
        
    def instalar(self):
        dlg=FileChooserDialog(parent=self.top,title="Instalar modulo",action=FILE_CHOOSER_ACTION_OPEN, buttons=("gtk-yes",1,"gtk-no",0))
        filter = FileFilter()
        filter.set_name("Python Scripts")
        filter.add_pattern("*.py")
        dlg.add_filter(filter)
        try:
            if dlg.run():
                dados=open(dlg.get_filename()).read()
                saida=os.path.join(self.path,os.path.split(dlg.get_filename())[-1])
                if os.path.exists(saida):
                    dlask=MessageDialog(self.top,0,MESSAGE_INFO,BUTTONS_YES_NO,"Modulos já existe sobrescrer?")
                    if dlask.run()!=RESPONSE_YES:
                        dlg.destroy();dlask.destroy()
                        return
                open(saida,'w').write(dados)
                self.atualizar()
        except Exception,e:
            print(e)
        dlg.destroy()

    def remover(self):
        md,it=self.lista.get_selection().get_selected()
        if it==None:return
        nome=md.get(it,0)[0]
        filename=os.path.join(self.path,nome)
        dlg = MessageDialog(self.top,0,MESSAGE_INFO,BUTTONS_YES_NO,"Apagar o modulo %s"%nome)
        if dlg.run()==RESPONSE_YES:
            os.unlink(filename)
            self.atualizar()
        dlg.destroy()
                        
    def atualizar(self):
        lista= glob.glob(os.path.join(self.path,"*.py"))
        md = ListStore(str)
        for modulo in lista:
            md.append([modulo.replace(os.path.join(self.path,""),"")])
        self.lista.set_model(md)
        
    def rodar(self,evt,idx,cln):
	md,it = self.lista.get_selection().get_selected()
        if it==None:return
        modulo=os.path.join(self.path,md.get(it,0)[0])
        if os.name=="nt":  
            cmd = "start %s"%modulo
        else:    
            cmd =  sys.executable+" "+modulo+" &"
        os.system(cmd)


if __name__=="__main__":
    jn=Window()
    jn.set_size_request(300,400)
    jn.connect("destroy",main_quit)
    jn.add(Modulos())
    jn.show_all()
    main()
