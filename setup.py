#
#-*-coding:utf-8-*-
#
nogui = 0
try:
    from gtk import *
except:
    nogui = 1

import compileall,glob,os,sys,zipfile,time,stat


class Setup:
	def build(self):
		self.apaga("*.pyc")
		self.apaga("*~")
		self.apaga("Agenda.zip")
		self.apaga("Agenda.sh")	
		compileall.compile_dir('.')
		open("Agenda.sh","w").write("#!/bin/bash\n\npython $0 &\nexit\n\n")
		saida = zipfile.ZipFile("Agenda.sh","a",zipfile.ZIP_DEFLATED)
		for nome in glob.glob("*.pyc"):saida.write(nome)
		main=zipfile.ZipInfo("__main__.py",str(time.time()))
		maindata="#\n#-*-coding:utf-8-*-\n\n\nfrom gtk import *\nfrom MainWindow import MainWindow\n\nif __name__=='__main__':\n    jn=MainWindow()\n    jn.show_all()\n    main()\n\n"
		saida.writestr( "__main__.py",maindata)
		self.apaga("*.pyc")
		#os.chmod("Agenda.sh",stat.S_IEXEC|)
		saida.close()
		os.system("chmod +x Agenda.sh")

	def clean(self):
		print("Apagando backups do editor")
		self.apaga("*~")
		print("Apagando arquivos compilados")
		self.apaga("*.pyc")
		print("Apagando executavel")
		self.apaga("Agenda.sh")
		print("pronto")

	def backup(self):        
		saida = zipfile.ZipFile("Agenda.zip","a",zipfile.ZIP_DEFLATED)
		for nome in glob.glob("*.py"):
				print("Copiando %s"%nome)
				saida.write(nome)
		#saida.write("db/agenda.db")
		saida.close()
		print("Arquivo de backup gerado")

	def apaga(self,arquivos):
		try:
			for arquivo in glob.glob(arquivos):os.remove(arquivo)
		except OSError,e:print(e)


if __name__=="__main__":        
    os.chdir(os.path.abspath(os.path.dirname(sys.argv[0])))
    s=Setup()
    if nogui:
        if len(sys.argv)<2:
            s.build()
        else:
            if hasattr(s,sys.argv[1]):
                getattr(s,sys.argv[1])()
            else:print("Comando selecionado não existe, use: %s [build|clean|backup]"%sys.argv[0])
    else:
        dlg = Dialog()
        dlg.add_buttons("Build",0,"Backup",1,"Clean",2,"gtk-cancel",3)
        ret= dlg.run()
        dlg.destroy()
        if ret<3:getattr(s,['build','backup','clean'][ret])()

