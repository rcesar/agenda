#
#-*-coding:utf-8-*-
import sys
sys.argv[0]="Agenda.py" #Muda o nome do programa na lista de janelas do gnome e do windows
from gtk import *
from Agenda import Agenda
from AgendaTelefonica import AgendaTelefonica
#from Clima import Clima
from Notas import Notas
from Modulos import Modulos
from Favoritos import Favoritos
from DatePicker import DatePicker
from Webservices import Webservices
from CryptoEditor import CryptoEditor
from Alarme import Alarme
from Calculadora import Calculadora
from ValidaCNH import ValidaCNH
from icone import icone

from sqlite3 import dbapi2 as sqlite
import os,time,gobject,datetime


class MainWindow(Window):
    def __init__(self):
        self.start_db()
        Window.__init__(self)
        self.stb = Frame()#Statusbar()
        img =gdk.pixbuf_new_from_xpm_data(icone)
        self.set_icon(img)
        self.web=Webservices(self)
        self.set_title("Agenda")
        self.desktop = Notebook()
        self.connect('delete-event',self.sair)
        self.connect("destroy",main_quit)
        bx = VBox(False,0)
        self.make_menu()
        bx.pack_start(self.desktop,True,True)
        self.desktop.append_page(Agenda(self),Label("Agenda"))
        self.desktop.append_page(Notas(self),Label("Notas"))
        self.desktop.append_page(AgendaTelefonica(self),Label("Telefones"))
        self.desktop.append_page(Favoritos(self),Label("Favoritos"))
        self.desktop.append_page(Modulos(self),Label("Modulos"))
       # self.desktop.append_page(Clima(self),Label("Previsão do tempo"))
        self.set_size_request(800,400)
        bx.pack_end(self.stb,False,True)
        self.add(bx)
        self.alarme = Alarme(self)
        self.relogio()
        gobject.timeout_add(1000,self.relogio)

    def make_menu(self):
        self.stb.set_shadow_type(SHADOW_ETCHED_IN)
        self.stb.set_border_width(1)
        tb = MenuBar()
        tb.set_size_request(-1,16)
        l =ToolButton()
        l.set_size_request(-1,16)
        l.connect("clicked",lambda evt:self.alarme.show())
        f = Frame()
        f.set_shadow_type(SHADOW_OUT)
        f.add(tb)
        bx = HBox()    
        bx.pack_end(f,False,False)
        bx.pack_end(l,False,False)
        self.stb.add(bx)
        self.stb.relogio = l
        it2 = MenuItem("Utilitarios")
        mmenu2=Menu()
        itens = [MenuItem("Calculo de digito verificador"),
                 MenuItem("Calculadora"),
                 MenuItem("Ferramenta de criptografia"),
                 MenuItem("Soma dias"),
                 MenuItem("Intervalo entre horarios"),
                 MenuItem("Validador de CNH"),
                 SeparatorMenuItem(),
                 ImageMenuItem('gtk-quit')]
        itens[0].connect('activate',lambda evt:self.calculo_dv())
        itens[1].connect('activate',lambda evt:Calculadora().show_all())
        itens[2].connect('activate',lambda evt:CryptoEditor().show())        
        itens[3].connect('activate',lambda evt:self.soma_dias())
        itens[4].connect('activate',lambda evt:self.horas())
	itens[5].connect("activate",lambda evt:ValidaCNH().show_all())
        itens[7].connect("activate",lambda evt:self.destroy())
        for item in itens:mmenu2.append(item)
        it2.set_submenu(mmenu2)
        tb.append(it2)

    def calculo_dv(self):
        dlg = Dialog("Calculo DV")
        dlg.add_button('gtk-ok',1)
        dlg.add_button('gtk-cancel',0)
        fr=Frame("Numero:")
        et = Entry()
        fr.add(et)
        dlg.vbox.pack_start(fr,True,True)
        dlg.show_all()
        if dlg.run():
            valor=et.get_text()
            soma=0
            controle=9
            for i in range(len(valor)-1,-1,-1):
                soma=soma+(controle*int(valor[i]))
                if controle>1:
                    controle-=1
                else:controle=9
            dv=str(soma%11)
            if dv=='10':dv='X'
            dl2 = MessageDialog(self,0,MESSAGE_INFO,BUTTONS_OK,valor+'-'+dv)
            dl2.run();dl2.destroy()
        dlg.destroy()

    def relogio(self):
        self.stb.relogio.set_label(time.strftime("%H:%M:%S "))
        self.alarme.check(time.strftime("%H:%M"))
        return(True)


    def start_db(self):
          sql = {"notas":"CREATE TABLE notas(titulo char(100),nota TEXT)",
                 "clientes":"CREATE TABLE clientes(codigo CHAR(12),nome CHAR(150))",
                 "compromissos":"CREATE TABLE compromissos(id integer primary key,descricao CHAR(150),data date,obs text,pendente INT(1))",
                 "pendencias":"CREATE TABLE pendencias(id integer primary key,descricao CHAR(150),obs text,pendente INT(1))",
                 "enderecos":"CREATE TABLE enderecos(codigo CHAR(12),tipo CHAR(40),rua CHAR(150),bairro CHAR(50),cidade CHAR(50),estado CHAR(2),cep CHAR(13))",
                 "telefones":"CREATE TABLE telefones(codigo CHAR(12),tipo CHAR(2),descricao CHAR(40),valor CHAR(100))",
                 "favoritos":"CREATE TABLE favoritos(categoria CHAR(100),descricao CHAR(255),url CHAR(255))",
                 "cidades":"CREATE TABLE cidades(codigo char(10),nome char(100))",
                 "config":"CREATE TABLE config(chave CHAR(40),valor CHAR(100))",
                 "alarme":"CREATE TABLE alarme(hora CHAR(5))"}
          #path = os.path.join(os.path.abspath(os.path.dirname(sys.argv[0])),"db","agenda.db")
          #dirname =os.path.dirname(os.path.abspath(sys.argv[0]))
          path=''
          if os.name=='nt':
              path=os.path.join(os.environ.get("HOMEDRIVE"),os.environ.get("HOMEPATH"),".db","agenda.db")
          else:path=os.path.join(os.environ.get("HOME"),".db","agenda.db") 
          dirname=os.path.dirname(path)
          if not(os.path.exists(dirname)):os.mkdir(dirname)
          self.db = sqlite.connect(path)
          self.db.text_factory = str
          for nome in self.db.execute("SELECT name FROM SQLITE_MASTER WHERE type='table'").fetchall():
            try:    
                sql.pop(nome[0])
            except KeyError,e:
                print("Apagando tabela %s removida"%nome)
                self.db.execute("DROP TABLE %s"%nome)
          for nome in  sql.keys():          
              print("Criando tabela %s"%nome)
              self.db.execute(sql[nome])

    def sair(self,evt,dk):
        dlg = MessageDialog(self,0,MESSAGE_INFO,BUTTONS_YES_NO,"Deseja encerrar a agenda?")
        ret=True
        if dlg.run()==RESPONSE_YES:
            ret=False
        dlg.destroy()
        return(ret)

    def alert(self,message='',tipo='info'):
        tipo=tipo.upper()
        tipo = {'ERROR':MESSAGE_ERROR,'WARNING':MESSAGE_WARNING,'INFO':MESSAGE_INFO}
        dlg=MessageDialog(self,0,MESSAGE_INFO,BUTTONS_OK,message)
        dlg.run();dlg.destroy()


    def horas(self):
        dlg = Dialog()
        fr = Frame("Hora inicial:")
        horaini = {"horas":Entry(),"minutos":Entry()}
        bx = HBox()
        bx.pack_start(horaini['horas'],True,True)
        bx.pack_start(horaini['minutos'],True,True)
        fr.add(bx)
        dlg.vbox.pack_start(fr,False,True)
        fr = Frame("Hora final:")
        horafim = {"horas":Entry(),"minutos":Entry()}
        bx = HBox()
        bx.pack_start(horafim['horas'],True,True)
        bx.pack_start(horafim['minutos'],True,True)
        fr.add(bx)
        dlg.vbox.pack_start(fr,False,True)
        dlg.add_buttons("gtk-ok",1,"gtk-cancel",0)
        dlg.show_all()
        if dlg.run():
            ini = int(horaini['horas'].get_text()) * 60 + int(horaini['minutos'].get_text())
            fim = int(horafim['horas'].get_text()) * 60 + int(horafim['minutos'].get_text())
            tm = fim-ini
            if tm<60:
                final = '00:%.2i'%tm
            else:
                horas = int(float(tm)/60)
                minutos= tm - (horas*60)
                final = '%.2i:%.2i'%(horas,minutos)
            self.alert(final)
        dlg.destroy()            

    def soma_dias(self):
        dlg = Dialog()
        date = DatePicker()
        dias = Entry()
        fr1 = Frame('Data:')
        fr2 = Frame('Dias:')
        dlg.vbox.pack_start(fr1,False,True)
        dlg.vbox.pack_start(fr2,False,True)
        fr1.add(date)
        fr2.add(dias)
        dlg.add_button('gtk-ok',1)
        dlg.add_button('gtk-cancel',0)
        dlg.show_all()
        dias.grab_focus()
        if dlg.run():
            try:
                date = map(int,date.get_date('d/m/y').split('/'))
                data = datetime.date(day=date[0],month=date[1],year=date[2])
                data+=datetime.timedelta(days=int(dias.get_text()))
                data = '%.02i/%.2i/%i'%(data.day,data.month,data.year)
                msg = MessageDialog(parent=self,buttons=BUTTONS_OK)
                msg.set_markup('Data:\n%s'%data)
                dlg.destroy()
                msg.run();msg.destroy()
            except  Exception,e: print(e)
        else:dlg.destroy()        
        

if __name__=="__main__":
        jn=MainWindow()
        jn.show_all()
        main()


