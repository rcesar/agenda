#
#-*-coding:utf-8-*-
#

from gtk import *
import time


class DateEntry(Entry):
    def __init__(self):
        Entry.__init__(self)
        self.connect("insert_text",self.validar)
        self.connect("key_press_event",self.testa_comandos)

    def testa_comandos(self,entry,evento):#Teclas de atalho
        keyname = gdk.keyval_name(evento.keyval)
        if keyname.lower()=='delete':self.set_text('')
        
    def validar(self,entry,text,*args):      
        entry.stop_emission("insert_text")#evita o processamento normal do sinal
        result = ''.join([c for c in text if c in '0123456789'])
        texto=self.formata((entry.get_text()+result))
        entry.handler_block_by_func(self.validar)#Bloqueia o sinal para evitar recursividade
        entry.set_text(texto)
        entry.handler_unblock_by_func(self.validar)

    def formata(self,texto):
        texto=''.join([c for c in texto if c in '0123456789'])
        if len(texto)>=2:
            texto=texto[0:2]+'/'+texto[2:]
        if len(texto)>=5:
            texto=texto[0:5]+'/'+texto[5:]
        if len(texto)>10:texto=texto[0:10]
        return(texto)


class DatePicker(Frame):
    def __init__(self,text=''):
        if text=='':
            Frame.__init__(self)
        else:Frame.__init__(self,text)
        bx = HBox(False,0)
        self.__et =DateEntry()
        #self.__et.set_editable(False)
        image = Image()
        image.set_from_pixbuf(gdk.pixbuf_new_from_xpm_data(["16 16 9 1",
                    " 	c None",
                    ".	c #000000",
                    "+	c #E012EA",
                    "@	c #8F599F",
                    "#	c #FFFFFF",
                    "$	c #9A2424",
                    "%	c #45C174",
                    "&	c #F9F609",
                    "*	c #48CFDB",
                    "                ",
                    " .............. ",
                    " .+++++++++++@. ",
                    " .@@@@@@@@@@@@. ",
                    " .............. ",
                    " .############. ",
                    " .#$#%$&#$%#$#. ",
                    " .##*###*##*##. ",
                    " .#$#%$&#$%#$#. ",
                    " .##*###*##*##. ",
                    " .#$#%$&#$%#$#. ",
                    " .##*###*##*##. ",
                    " .#$#%$&#$%#$#. ",
                    " .############. ",
                    " .............. ",
                    "                "]))
        bt =ToolButton(image)
        bt.connect("clicked",self.__set_date)
        bx.pack_start(self.__et,True,True)
        bx.pack_end(bt,False,False)
        self.add(bx)
        self.__et.set_text(time.strftime("%d/%m/%Y"))
        self.set_shadow_type(SHADOW_NONE)

    def set(self,data):
        ano,mes,dia = data.split('-')
        data = '%s/%s/%s'%(dia,mes,ano)
        self.__et.set_text(data)

    def get(self):
        return(self.get_date('y-m-d'))

    def get_date(self,format="d/m/y"):
        '''possible date formats d/m/y y-m-d '''
        dia,mes,ano=self.__et.get_text().split("/")
        return(format.replace("d",dia).replace("m",mes).replace("y",ano))
                
    def __set_date(self,evt):
        dlg=Dialog()
        dlg.set_title("Selecione a data:")
        dia,mes,ano = map(int,self.__et.get_text().split("/"))
        mes-=1       
        dlg.add_button("gtk-ok",1)
        dlg.add_button("gtk-cancel",0)
        cld=Calendar()
        cld.select_day(dia)
        cld.select_month(mes,ano)
        dlg.vbox.add(cld);dlg.show_all();
        if dlg.run():
            ano,mes,dia =cld.get_date()
            mes+=1
            self.__et.set_text("%.2i/%.2i/%s"%(dia,mes,ano))
        dlg.destroy()
        
if __name__=="__main__":
    jn=Window()
    fr = Frame("Teste do Date Picker:")
    jn.add(fr)
    jn.connect("destroy",main_quit)
    fr.add(DatePicker())
    jn.show_all()
    main()
        
