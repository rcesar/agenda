#
#-*-coding:utf-8-*-
#
from gtk import *
import time

class AgendaTelefonica(Frame):
    def __init__(self,parent):
        self.db=parent.db
        self.top=parent
        Frame.__init__(self)
        fr1= Frame()
        bbox = VBox(False,0);fr1.add(bbox)
        self.lista=TreeView()
        frb = Frame("Localizar cliente:")
        frb.set_shadow_type(SHADOW_NONE)
        self.pad = Entry()
        bt1= ToolButton("gtk-find");bt1.connect("clicked",self.busca)
        tbx = HBox(False,0)
        tbx.pack_start(self.pad,True,True)
        tbx.pack_end(bt1,False,False)
        frb.add(tbx)
        bbox.pack_start(frb,False,True)
        scw = ScrolledWindow()
        scw.set_policy(POLICY_AUTOMATIC,POLICY_AUTOMATIC)
        scw.add(self.lista)
        bbox.pack_start(scw,True,True)
        tb = Toolbar()
        bt1 = ToolButton("gtk-save")
        bt2 = ToolButton("gtk-new")
        bt3 = ToolButton("gtk-delete")
        bt1.connect("clicked",self.salvar_cliente)
        bt2.connect("clicked",lambda evt:self.novo_cliente())
        bt3.connect("clicked",lambda evt:self.apagar_cliente())
        tb.add(bt1)
        tb.add(bt2)
        tb.add(SeparatorToolItem())
        tb.add(bt3)
        bbox.pack_end(tb,False,True)
        for c in [TreeViewColumn("Código",CellRendererText(),text=0),TreeViewColumn("Nome"+20*" ",CellRendererText(),text=1)]:self.lista.append_column(c)
        self.lista.set_model(ListStore(str,str))
        self.lista.connect("row-activated",self.abre_cliente)
        #
        fr2 = Frame()
        self.codigo=Entry()
        self.nome=Entry()
        plc = Table(3,3,0)
        plc.attach(self.put_in_frame(self.codigo,"Código:"),0,1,0,1,FILL,SHRINK)
        plc.attach(self.put_in_frame(self.nome,"Nome:"),1,4,0,1,EXPAND|FILL,SHRINK)
        dsk = Notebook()
        dsk.append_page(self.fr_enderecos(),Label("Endereços:"))
        dsk.append_page(self.fr_telefones(),Label("Telefones:"))
        dsk.append_page(self.fr_internet(),Label("Internet:"))
        plc.attach(dsk,0,4,1,2,EXPAND|FILL)
        fr2.add(plc)
        #
        hb = HBox(False,0)
        fr1.set_shadow_type(SHADOW_NONE)
        fr2.set_shadow_type(SHADOW_NONE)
        hb.pack_start(fr1,False,True)        
        hb.pack_start(fr2,True,True)        
        self.add(hb)
        self.codigo.set_property("editable",False)
        self.codigo.set_size_request(12,-1)

    def fr_enderecos(self):
        self.enderecos = TreeView()
        self.enderecos.connect("row-activated",self.abre_endereco)
        for c in [TreeViewColumn("Desc.:",CellRendererText(),text=0),
                  TreeViewColumn("Valor:",CellRendererText(),text=1)]:self.enderecos.append_column(c)
        self.enderecos.set_headers_visible(False)
        tb = Toolbar()
        vb = VBox(False,0)
        bts = [ ToolButton("gtk-new"),ToolButton("gtk-delete")]
        bts[0].connect("clicked",self.novo_endereco)
        bts[1].connect("clicked",self.apaga_endereco)
        for bt in bts:tb.add(bt)
        scw = ScrolledWindow()
        scw.set_policy(POLICY_AUTOMATIC,POLICY_AUTOMATIC)
        scw.add(self.enderecos)
        vb.pack_start(scw,True,True)
        vb.pack_end(tb,False,True)
        fr = Frame()
        fr.set_shadow_type(SHADOW_NONE)        
        fr.add(vb)
        return(fr)

    def fr_telefones(self):
        self.telefones = TreeView()
        self.telefones.connect("row-activated",self.abre_telefone)
        for c in [TreeViewColumn("Desc.:",CellRendererText(),text=0),
                  TreeViewColumn("Valor:",CellRendererText(),text=1)]:self.telefones.append_column(c)
        self.telefones.set_headers_visible(False)
        tb = Toolbar()
        vb = VBox(False,0)
        bts = [ ToolButton("gtk-new"),ToolButton("gtk-delete")]
        bts[0].connect("clicked",self.add_telefone)
        bts[1].connect("clicked",self.apaga_telefone)
        for bt in bts:tb.add(bt)
        scw = ScrolledWindow()
        scw.set_policy(POLICY_AUTOMATIC,POLICY_AUTOMATIC)
        scw.add(self.telefones)
        vb.pack_start(scw,True,True)
        vb.pack_end(tb,False,True)
        fr = Frame()
        fr.set_shadow_type(SHADOW_NONE)
        fr.add(vb)
        fr.set_shadow_type(SHADOW_NONE)                
        return(fr)

    def fr_internet(self):
        self.email = TreeView()
        self.email.connect("row-activated",self.abre_email)
        for c in [TreeViewColumn("Desc.:",CellRendererText(),text=0),
                  TreeViewColumn("Valor:",CellRendererText(),text=1)]:self.email.append_column(c)
        self.email.set_headers_visible(False)
        tb = Toolbar()
        vb = VBox(False,0)
        bts = [ ToolButton("gtk-new"),ToolButton("gtk-delete")]
        bts[0].connect("clicked",self.add_email)
        bts[1].connect("clicked",self.apaga_email)
        for bt in bts:tb.add(bt)
        scw = ScrolledWindow()
        scw.set_policy(POLICY_AUTOMATIC,POLICY_AUTOMATIC)
        scw.add(self.email)        
        vb.pack_start(scw,True,True)
        vb.pack_end(tb,False,True)
        fr = Frame()
        fr.add(vb)
        fr.set_shadow_type(SHADOW_NONE)                
        return(fr)

    def put_in_frame(self,wdg,label):
        fr=Frame(label)
        fr.set_shadow_type(SHADOW_NONE)                
        fr.add(wdg)
        return(fr)

    def busca(self,EVT):
        md = ListStore(str,str)
        pad=self.pad.get_text()
        for ret in self.db.execute("SELECT codigo,nome FROM clientes WHERE nome like ?",(self.pad.get_text()+"%",)).fetchall():
            md.append(ret)
        self.lista.set_model(md)

    def novo_cliente(self):
        self.codigo.set_text("")
        self.nome.set_text("")
        self.enderecos.set_model(ListStore(str,str))
        self.telefones.set_model(ListStore(str,str))
        self.email.set_model(ListStore(str,str))

    def apagar_cliente(self):
        md,it = self.lista.get_selection().get_selected()
        if it==None:return
        codigo = md.get_value(it,0)
        nome = md.get_value(it,1)
        dlg=MessageDialog(self.top,DIALOG_MODAL,MESSAGE_QUESTION,BUTTONS_YES_NO,"Apagar os dados de:%s"%nome)
        if dlg.run()==RESPONSE_YES:
            self.db.execute("DELETE FROM clientes WHERE codigo=?",(codigo,))
            self.db.execute("DELETE FROM enderecos WHERE codigo=?",(codigo,))
            self.db.execute("DELETE FROM telefones WHERE codigo=?",(codigo,))
            self.db.commit()
            if self.codigo.get_text()==codigo:self.novo_cliente()
        dlg.destroy()

    def abre_cliente(self,wdg,idx,col):
        md,it = self.lista.get_selection().get_selected()
        if it==None:return
        self.codigo.set_text(md.get_value(it,0))        
        self.nome.set_text(md.get_value(it,1))
        self.atualizar_enderecos()
        self.atualizar_telefones()

    def atualizar_enderecos(self):
        codigo=self.codigo.get_text()
        md = ListStore(str,str)
        for endereco in self.db.execute("SELECT tipo,rua,bairro,cidade,estado,cep FROM enderecos WHERE codigo=?",(codigo,)).fetchall():
            txt = "Rua:%s\nBairro:%s Cidade:%s\nEstado:%s CEP:%s"%(endereco[1],endereco[2],endereco[3],endereco[4],endereco[5])
            valor = [endereco[0],txt]
            md.append(valor)
        self.enderecos.set_model(md)
        
        
    def apaga_endereco(self,evt):
        md,it = self.enderecos.get_selection().get_selected()
        if it==None:return
        descricao = md.get_value(it,0)
        dlg = MessageDialog(self.top,DIALOG_MODAL,MESSAGE_QUESTION,BUTTONS_YES_NO,"Apagar o endereço:%s para o cliente?"%descricao)
        if dlg.run()==RESPONSE_YES:
            self.db.execute("DELETE FROM enderecos WHERE tipo=? AND codigo=?",(descricao,self.codigo.get_text()))
            self.db.commit()
            self.atualizar_enderecos()
        dlg.destroy()
        
        
    def novo_endereco(self,evt):
        if self.codigo.get_text()=='':return
        dlg=DLGEndereco(self)
        if dlg.run():
            ct =self.db.execute("SELECT COUNT(tipo) FROM enderecos WHERE codigo=? AND tipo=?",(self.codigo.get_text(),dlg.tipo.get_text())).fetchone()[0]
            if ct<=0:
                self.db.execute("INSERT INTO ENDERECOS(codigo,tipo,rua,bairro,cidade,cep,estado) VALUES(?,?,?,?,?,?,?)",
                (self.codigo.get_text(),dlg.tipo.get_text(),dlg.rua.get_text(),dlg.bairro.get_text(),dlg.cidade.get_text(),dlg.cep.get_text(),dlg.estado.get_text()))
                self.db.commit()
                self.atualizar_enderecos()
        dlg.destroy()            
        
    def abre_endereco(self,wdg,idx,col):
        md,it = self.enderecos.get_selection().get_selected()
        if it==None:return
        descricao = md.get_value(it,0)
        rua,bairro,cidade,cep,estado = self.db.execute('SELECT rua,bairro,cidade,cep,estado FROM enderecos WHERE codigo=? AND tipo=?',(self.codigo.get_text(),descricao)).fetchone()
        dlg = DLGEndereco(self)
        dlg.tipo.set_text(descricao)
        dlg.rua.set_text(rua)
        dlg.bairro.set_text(bairro)
        dlg.cidade.set_text(cidade)
        dlg.cep.set_text(cep)
        dlg.estado.set_text(estado)
        dlg.tipo.set_property("editable",False)
        if dlg.run():
            self.db.execute('UPDATE enderecos SET rua=?,bairro=?,cidade=?,cep=?,estado=? WHERE tipo=?',(
                            dlg.rua.get_text(),dlg.bairro.get_text(),dlg.cidade.get_text(),dlg.cep.get_text(),dlg.estado.get_text(),descricao))
            self.db.commit()
            self.atualizar_enderecos()
        dlg.destroy()
    
    def salvar_cliente(self,evt):
        if self.codigo.get_text()=='':
            self.codigo.set_text(str(int(time.time()*100))[-12:])
            self.db.execute("INSERT INTO clientes VALUES(?,?)",(self.codigo.get_text(),self.nome.get_text()))
        else:
            self.db.execute("UPDATE clientes SET nome=? WHERE codigo=?",(self.nome.get_text(),self.codigo.get_text()))
        self.db.commit()
   
    def abre_telefone(self,wdg,idx,col):
        md,it = self.telefones.get_selection().get_selected()
        if it==None:return
        descricao = md.get_value(it,0)
        valor =md.get_value(it,1)
        dlg = DLGTelefone(self)
        dlg.descricao.set_text(descricao)
        dlg.descricao.set_property("editable",False)
        dlg.valor.set_text(valor)
        if dlg.run():
           self.db.execute("UPDATE telefones SET valor=? WHERE tipo='telefone' AND codigo=? AND descricao=?",
                            (dlg.valor.get_text(),self.codigo.get_text(),descricao))
           self.db.commit()
           self.atualizar_telefones()
        dlg.destroy()
        
    
    def abre_email(self,wdg,idx,col):
        md,it = self.email.get_selection().get_selected()
        if it==None:return
        descricao = md.get_value(it,0)
        valor =md.get_value(it,1)
        dlg = DLGTelefone(self,'Email')
        dlg.descricao.set_text(descricao)
        dlg.descricao.set_property("editable",False)
        dlg.valor.set_text(valor)
        if dlg.run():
           self.db.execute("UPDATE telefones SET valor=? WHERE tipo='email' AND codigo=? AND descricao=?",
                            (dlg.valor.get_text(),self.codigo.get_text(),descricao))
           self.db.commit()
           self.atualizar_telefones()
        dlg.destroy()    

    def apaga_telefone(self,evt):
        md,it = self.telefones.get_selection().get_selected()
        if it==None:return
        descricao = md.get_value(it,0)
        valor =md.get_value(it,1)
        dlg = MessageDialog(self.top,DIALOG_MODAL,MESSAGE_QUESTION,BUTTONS_YES_NO,"Apagar o telefone:\n%s\n%s"%(descricao,valor))
        if dlg.run()==RESPONSE_YES:
            self.db.execute("DELETE FROM telefones WHERE codigo=? AND tipo='telefone' AND descricao=?",(self.codigo.get_text(),descricao))
            self.db.commit()
            self.atualizar_telefones()
        dlg.destroy()

    def apaga_email(self,evt):
        md,it = self.email.get_selection().get_selected()
        if it==None:return
        descricao = md.get_value(it,0)
        valor =md.get_value(it,1)
        dlg = MessageDialog(self.top,DIALOG_MODAL,MESSAGE_QUESTION,BUTTONS_YES_NO,"Apagar o email:\n%s\n%s"%(descricao,valor))
        if dlg.run()==RESPONSE_YES:
            self.db.execute("DELETE FROM telefones WHERE codigo=? AND tipo='email' AND descricao=?",(self.codigo.get_text(),descricao))
            self.db.commit()
            self.atualizar_telefones()
        dlg.destroy()
    
                 
    def atualizar_telefones(self):
        mdt = ListStore(str,str)
        mdi = ListStore(str,str)
        for dado in self.db.execute("SELECT descricao,valor,tipo FROM telefones WHERE codigo=? ORDER BY tipo",(self.codigo.get_text(),)).fetchall():
            if dado[2]=='telefone':
                mdt.append([dado[0],dado[1]])
            else:mdi.append([dado[0],dado[1]])
        self.telefones.set_model(mdt)
        self.email.set_model(mdi)

    def add_telefone(self,evt):
        dlg = DLGTelefone(self)
        if self.codigo.get_text()=='':return
        if dlg.run() and self.db.execute("SELECT count(descricao) FROM telefones WHERE tipo=? AND codigo=? AND descricao=?",("telefone",self.codigo.get_text(),dlg.descricao.get_text())).fetchone()[0]<=0:
            self.db.execute("INSERT INTO telefones(codigo,tipo,descricao,valor) VALUES(?,'telefone',?,?)",(
                            self.codigo.get_text(),dlg.descricao.get_text(),dlg.valor.get_text()))
            self.db.commit()
            self.atualizar_telefones()
        dlg.destroy()

    def add_email(self,evt):
        dlg = DLGTelefone(self,'Email')
        if self.codigo.get_text()=='':return
        if dlg.run() and self.db.execute("SELECT count(descricao) FROM telefones WHERE tipo=? AND codigo=? AND descricao=?",("email",self.codigo.get_text(),dlg.descricao.get_text())).fetchone()[0]<=0:
            self.db.execute("INSERT INTO telefones(codigo,tipo,descricao,valor) VALUES(?,'email',?,?)",(
                            self.codigo.get_text(),dlg.descricao.get_text(),dlg.valor.get_text()))
            self.db.commit()
            self.atualizar_telefones()
        dlg.destroy()

class DLGEndereco(Dialog):
    def __init__(self,parent):
        Dialog.__init__(self)
        self.set_title("Endereço")
        tbl = Table(2,4)
        self.tipo=Entry()
        self.rua=Entry()
        self.bairro=Entry()
        self.cidade=Entry()
        self.cep=Entry()
        self.estado=Entry()
        tbl.attach(parent.put_in_frame(self.tipo,"Tipo:"),0,2,0,1)
        tbl.attach(parent.put_in_frame(self.rua,"Rua:"),0,2,1,2)
        tbl.attach(parent.put_in_frame(self.bairro,"Bairro:"),0,1,2,3)        
        tbl.attach(parent.put_in_frame(self.cidade,"Cidade:"),1,2,2,3)
        tbl.attach(parent.put_in_frame(self.cep,"CEP:"),0,1,3,4)
        tbl.attach(parent.put_in_frame(self.estado,"Estado:"),1,2,3,4)
        self.vbox.add(tbl)
        self.add_button("gtk-ok",1)
        self.add_button("gtk-cancel",0)
        self.show_all()

class DLGTelefone(Dialog):
    def __init__(self,parent,tipo="Telefone:"):
        Dialog.__init__(self)
        self.tipo=tipo.lower()
        self.descricao = Entry()
        self.valor = Entry()
        self.vbox.pack_start(parent.put_in_frame(self.descricao,"Tipo:"))
        self.vbox.pack_start(parent.put_in_frame(self.valor,tipo))
        self.add_button("gtk-ok",1)
        self.add_button("gtk-cancel",0)
        self.show_all()
