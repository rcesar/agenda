#
#-*-coding:utf-8-*-
import urllib2
import urllib 
import sys
from gtk import *
import tempfile,os


class Clima(Frame):
    def __init__(self,parent):
        self.db = parent.db
        self.top=parent
        Frame.__init__(self)
        scw = ScrolledWindow()
        self.lista = TreeView()
        self.lista.append_column(TreeViewColumn("",CellRendererPixbuf(),pixbuf=0))
        self.lista.append_column(TreeViewColumn("",CellRendererText(),text=1))
        self.lista.set_headers_visible(False)
        bx = VBox(0,0)
        tb = Toolbar()
        tb.set_style(TOOLBAR_BOTH)
        bt1 = ToolButton("gtk-edit");tb.add(bt1)
        bt1.connect("clicked",lambda evt:ListaCidades(self.top))
        bt1.set_label("Edita lista das cidades")
        bt2 = ToolButton("gtk-refresh");tb.add(bt2)
        bt2.connect("clicked",lambda evt:self.atualizar())
        bx.pack_start(tb,False,True)
        bx.pack_end(scw,True,True)
        scw.add(self.lista)
        scw.set_policy(POLICY_AUTOMATIC,POLICY_AUTOMATIC)
        self.add(bx)


    def atualizar(self):
        baseurl = "http://www.cptec.inpe.br/widget/widget.php?p=%s&w=p&c=909090&f=ffffff"
        ret = []
        md = ListStore(gdk.Pixbuf,str)
        for cidade in self.db.execute("SELECT codigo,nome FROM cidades ORDER BY nome").fetchall():
            url = baseurl%cidade[0]
            dados=self.ler(url)
            ininome = dados.find("<div class=\"cidade\">")+21
            fimnome = ininome+dados[ininome:].find("</div>")
            ininome = ininome+dados[ininome:fimnome].find("title=")+7
            fimnome = ininome+dados[ininome:fimnome].find("\"")
            nome = dados[ininome:fimnome]
            ini=dados.find("img src=")+9
            fim=ini+dados[ini:].find('"')
            inialt=ini+dados[ini:].find("alt=")+5
            fimalt=inialt+dados[inialt:].find("\"")
            legenda = dados[inialt:fimalt]
            img = self.make_image(dados[ini:fim])
            initemp = dados.find("<div class=\"temperatures\">")+26
            fimtemp = dados[initemp:].find("</div>")+initemp
            temperaturas = dados[initemp:fimtemp]
            texto = self.limpa_html(nome)+"\n"+legenda+"\n"+self.limpa_html(temperaturas)
            md.append([img,texto])
        self.lista.set_model(md)

    def make_image(self,imgname):
        url="http://www.cptec.inpe.br/widget/"+imgname
        try:
            loader = gdk.PixbufLoader()
            loader.write(self.ler(url))
            loader.close()
#            img =  Image()
 #           img.set_from_pixbuf(loader.get_pixbuf())
            img = loader.get_pixbuf()            
        except Exception,e:
            self.alert(e)
        return(img)

    def alert(self,err,ic=MESSAGE_ERROR):
        dlg = MessageDialog(buttons=BUTTONS_OK,type=ic)
        dlg.set_markup(str(err))
        dlg.run();dlg.destroy()
 
    def ler(self,url,dd=None):        
        op=self.get_opener()
        dados=''
        try:           
            dados=op(url).read()
        except Exception,e:
            self.alert(str(e)+"\n"+dados,'ERROR')        
            return({})
        return(dados)

    def get_opener(self):
        proxy= self.db.execute("SELECT valor FROM config WHERE chave='proxy'").fetchone()[0]
        usuario= self.db.execute("SELECT valor FROM config WHERE chave='proxy_user'").fetchone()[0]
        senha = self.db.execute("SELECT valor FROM config WHERE chave='proxy_password'").fetchone()[0]
        if proxy.lower()=='none':
            opener=urllib2.urlopen
        else:
            opener=self.pxhandler({'servidor':proxy,'usuario':usuario,'senha':senha})
        return(opener)

    def pxhandler(self,px):
        servidor='http://'+px['usuario']+":"+px['senha']+"@"+px['servidor']
        proxy=urllib2.ProxyHandler({'http':servidor})
        auth=urllib2.HTTPBasicAuthHandler()
        return(urllib2.build_opener(proxy,auth,urllib2.HTTPHandler).open)

    def limpa_html(self,texto):
        texto = texto.replace("&nbsp;"," ").replace("&aacute;","à").replace("<br>","\n").replace("<br />","").replace("&ordm;","º")
        texto=texto.replace("&atilde;","ã")
        return(texto)
            

class ListaCidades(Window):
    def __init__(self,top):
        self.db = top.db
        Window.__init__(self)
        self.set_size_request(400,400)
        bx=VBox(0,0);self.add(bx)
        self.lista=TreeView()
        tb = Toolbar()
        bt1 = ToolButton("gtk-close");bt1.connect("clicked",lambda evt:self.destroy())
        tb.add(bt1)
        tb.add(SeparatorToolItem())
        bt2 = ToolButton("gtk-new");bt2.connect("clicked",lambda evt:self.nova())
        bt3 = ToolButton("gtk-delete");bt3.connect("clicked",lambda evt:self.apagar())
        tb.add(bt2)
        tb.add(bt3)
        bx.pack_start(tb,False,True)
        bx.pack_end(self.lista,True,True)
        self.lista.append_column(TreeViewColumn("Código:",CellRendererText(),text=0))
        self.lista.append_column(TreeViewColumn("Nome:",CellRendererText(),text=1))
        self.atualizar()
        self.show_all()

    def atualizar(self):
        md = ListStore(str,str)
        for cidade in self.db.execute("SELECT * FROM cidades ORDER BY nome").fetchall():md.append(cidade)
        self.lista.set_model(md)

    def nova(self):
        dlg = DLGCidade()
        if dlg.run() and self.db.execute("SELECT count(codigo) FROM cidades WHERE codigo=?",(dlg.codigo.get_text(),)).fetchone()[0]<=0:
            try:
                self.db.execute("INSERT INTO cidades VALUES(?,?)",(dlg.codigo.get_text(),dlg.cidade.get_text()))
                self.db.commit()
            except Exception,e:
                dlg2=MessageDialog(parent=self,buttons=BUTTONS_OK)
                dlg2.set_markup(str(e))
                dlg2.run();dlg2.destroy()
        dlg.destroy()
        self.atualizar()

    def apagar(self):
        md,it=self.lista.get_selection().get_selected()
        if it==None:return
        codigo=md[it][0]
        dlg = MessageDialog(parent=self,buttons=BUTTONS_YES_NO)
        dlg.set_markup("Apagar a cidade selecionada?")
        if dlg.run()==RESPONSE_YES:
            self.db.execute("DELETE FROM cidades WHERE codigo=?",(codigo,))
            self.db.commit()
        self.atualizar()
                        
class DLGCidade(Dialog):
    def __init__(self):
        Dialog.__init__(self)
        self.codigo=Entry()
        fr=Frame("Código:");fr.add(self.codigo)
        self.vbox.pack_start(fr,False,True)
        self.cidade=Entry()
        fr=Frame("Cidade:");fr.add(self.cidade)
        self.vbox.pack_start(fr,False,True)
        self.add_buttons("gtk-ok",1,"gtk-cancel",0)
        self.show_all()

        
if __name__=="__main__":
    from sqlite3 import dbapi2
    db =dbapi2.connect(":memory:")
    db.execute("CREATE TABLE config(chave char(50),valor char(100))")
    db.execute("CREATE TABLE cidades(codigo char(10),nome char(100))")
    db.execute("INSERT INTO config VALUES('proxy_password','22270046')")   
    db.execute("INSERT INTO cidades VALUES('1932','Estiva Gerbi')")         
    db.execute("INSERT INTO cidades VALUES('3303','Mogi-Guaçu')")
    db.execute("INSERT INTO cidades VALUES('1182','Campinas')")
    db.execute("INSERT INTO cidades VALUES('5515','Ubatuba')")
    db.execute("INSERT INTO cidades VALUES('2169','Gramado')")
    class obj:pass
    p = obj()
    p.db=db
    jn=Window()
    jn.set_size_request(640,480)
    jn.add(PrevTemp(p))
    jn.connect("destroy",main_quit)
    jn.show_all()
    main()
    
