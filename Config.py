#
#-*-coding:utf-8-*-
#
from gtk import *

class ProxyConfig(Window):
    def __init__(self,parent):
        self.top=parent
        Window.__init__(self)
        self.set_title("Configuração de proxy")
        frp = Frame("Configuração de proxy")
        bx = VBox(False,0)
        fr,self.proxy = self.lentry("Servidor proxy:");bx.pack_start(fr,False,True)
        fr,self.usuario_proxy = self.lentry("Usuario proxy:");bx.pack_start(fr,False,True)
        fr,self.senha_proxy = self.lentry("Senha proxy:");bx.pack_start(fr,False,True)                
        fr,self.usuario=self.lentry("Usuario agenda:");bx.pack_start(fr,False,True)
        fr,self.senha=self.lentry("Senha agenda:");bx.pack_start(fr,False,True)
        self.senha_proxy.set_visibility(False)
        self.senha.set_visibility(False)
        frp.add(bx)
        self.add(frp)
        bx1 = HBox(True,0)
        bt = ToolButton("gtk-save");bt.connect("clicked",self.salvar)
        bt1 = ToolButton("gtk-close");bt1.connect("clicked",lambda evt:self.destroy())
        bx1.pack_start(bt,True,True)
        bx1.pack_start(bt1,True,True)        
        bx.pack_end(bx1)
        self.show_all()
        try:
            self.proxy.set_text(self.top.db.execute("SELECT valor FROM config WHERE chave='proxy'").fetchone()[0])
        except:self.proxy.set_text('None')
        try:
            self.usuario_proxy.set_text(self.top.db.execute("SELECT valor FROM config WHERE chave='proxy_user'").fetchone()[0])
        except:pass
        try:
            self.senha_proxy.set_text(self.top.db.execute("SELECT valor FROM config WHERE chave='proxy_password'").fetchone()[0])
        except:pass
        try:
            self.usuario.set_text(self.top.db.execute("SELECT valor FROM config WHERE chave='usuario'").fetchone()[0])
        except:pass
        try:
            self.senha.set_text(self.top.db.execute("SELECT valor FROM config WHERE chave='senha'").fetchone()[0])
        except:pass
        
        
        
    def salvar(self,evt):
        svd = self.proxy.get_text()
        user=self.usuario_proxy.get_text()
        senha=self.senha_proxy.get_text()
        t = self.top.db.execute('SELECT COUNT(chave) FROM config WHERE chave=?',('proxy',)).fetchone()[0]
        if t ==0:        
            self.top.db.execute("INSERT INTO config VALUES(?,?)",('proxy',svd))
            self.top.db.execute("INSERT INTO config VALUES(?,?)",('proxy_user',user))
            self.top.db.execute("INSERT INTO config VALUES(?,?)",('proxy_password',senha))            
            self.top.db.execute("INSERT INTO config VALUES(?,?)",('usuario',self.usuario.get_text()))
            self.top.db.execute("INSERT INTO config VALUES(?,?)",('senha',self.senha.get_text()))            
        else:
            self.top.db.execute("UPDATE config SET valor=? WHERE chave=?",(svd,'proxy'))
            self.top.db.execute("UPDATE config SET valor=? WHERE chave=?",(user,'proxy_user'))
            self.top.db.execute("UPDATE config SET valor=? WHERE chave=?",(senha,'proxy_password'))            
            self.top.db.execute("UPDATE config SET valor=? WHERE chave=?",(self.usuario.get_text(),'usuario'))
            self.top.db.execute("UPDATE config SET valor=? WHERE chave=?",(self.senha.get_text(),'senha'))            
        self.top.db.commit()
        
    def lentry(self,label=''):
        fr = Frame(label)
        fr.set_shadow_type(SHADOW_NONE)
        et = Entry()
        fr.add(et)
        return(fr,et)
        
        
if __name__=="__main__":execfile("MainWindow.py",{'__name__':'__main__'})
