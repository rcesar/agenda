#
#-*-coding:utf-8-*-
#
from gtk import *


class LEntry(Frame):
    def __init__(self,title):
        Frame.__init__(self,title)
        self._t=Entry()
        self.add(self._t)
        for attr in ['get_text','set_text']:
            setattr(self,attr,getattr(self._t,attr))


class CadAluno(Dialog):
    def __init__(self,parent):
        Dialog.__init__(self,parent=parent)
        self.set_title("Cadastro de alunos")
        self.set_size_request(600,-1)
        self.codigo=LEntry('Matricula')
        self.nome=LEntry('Nome')
        self.telefone=LEntry('Telefone')
        self.rua=LEntry('Rua')
        self.bairro=LEntry('Bairro')
        self.cidade=LEntry('Cidade')
        bx1=HBox(False,False)
        bx1.pack_start(self.codigo,False,False)
        bx1.pack_end(self.nome,True,True)
        self.vbox.pack_start(bx1,False,True)
        self.vbox.pack_start(self.telefone,False,False)
        self.vbox.pack_start(self.rua,False,True)
        bx2=HBox(False,False)
        bx2.pack_start(self.bairro,True,True)
        bx2.pack_start(self.cidade,True,True)
        self.vbox.pack_start(bx2,False,True)
        self.add_button('gtk-ok',1)
        self.add_button('gtk-cancel',0)
        self.show_all()

class Academia(Window):
    def __init__(self):
        Window.__init__(self)
        self.set_title("Gestão de alunos")
        self.set_size_request(800,600)
        bx=VBox(False,False)
        #bx.pack_start(self.make_menu(),False,False)
        bx.pack_start(self.make_toolbar(),False,False)
        self.lista=TreeView()
        self.lista.append_column(TreeViewColumn("Matricula",CellRendererText(),text=0))
        self.lista.append_column(TreeViewColumn("Nome",CellRendererText(),text=1))
        self.lista.append_column(TreeViewColumn("",CellRendererText(),text=2))
        self.lista.append_column(TreeViewColumn("",CellRendererText(),text=3))
        self.lista.append_column(TreeViewColumn("",CellRendererText(),text=4))
        self.lista.append_column(TreeViewColumn("",CellRendererText(),text=5))
        self.lista.append_column(TreeViewColumn("",CellRendererText(),text=6))
        self.lista.append_column(TreeViewColumn("",CellRendererText(),text=7))
        self.lista.set_model(ListStore(str,str,str,str,str,str,str,str))
        bx.pack_start(self.lista,True,True)
        self.add(bx)

    def make_toolbar(self):
        tb=Toolbar()
        bt1=ToolButton('gtk-quit');bt1.connect("clicked",lambda evt:self.sair())
        img=gdk.pixbuf_new_from_xpm_data(figuras['pessoa'])
        im=Image()
        im.set_from_pixbuf(img)
        bt2=ToolButton(im);bt2.connect("clicked",lambda evt:self.novo_aluno())
        bt2.set_label("Cadastro de alunos")
        #t2.set_icon_widget(img)
        tb.add(bt1)
        tb.add(SeparatorToolItem())
        tb.add(bt2)
        return(tb)

    def novo_aluno(self):
        dlg=CadAluno(self)
        if dlg.run():
            print("SALVAR")
        dlg.destroy()

    def sair(self):main_quit()
figuras={

"pessoa":[ "20 20 2 1",
" 	c None",
".	c #000000",
"                    ",
"                    ",
"                    ",
"                    ",
"       .......      ",
"     ...........    ",
"    .............   ",
"    .............   ",
"    .............   ",
"    .............   ",
"     ...........    ",
"       .......      ",
"         ....       ",
"         .....      ",
"       ........     ",
"   ................ ",
"  ..................",
" ...................",
"....................",
"...................."]}

if __name__=="__main__":
    jn=Academia()
    jn.connect("destroy",main_quit)
    jn.show_all()
    main()
        
        
