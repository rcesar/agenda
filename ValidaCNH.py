#
#-*-coding:utf-8-*-
#
from gtk import *

def valida_cnh(cnh):
	cnh=cnh
	ret=[False]
	numero=cnh[:9]
	soma=0
	muda_digito_segundo=0
	for i in range(8,0,-1):
		soma+=int(numero[i])*(10-(i+1))
	primeiro_digito=soma%11
	if primeiro_digito>=10:
		muda_digito_segundo=-2
		primeiro_digito=0
	psoma=0	
	for i in range(9,0,-1):
		psoma+=int(numero[i-1])*(i)
	segundo_digito=psoma%11
	if segundo_digito>=10:
		segundo_digito=0
	segundo_digito+=muda_digito_segundo
	digito_verificador=str(primeiro_digito)+str(segundo_digito)	
	ret=[(digito_verificador==cnh[9:]),cnh,digito_verificador]
	return(ret)


class ValidaCNH(Window):
	def __init__(self):
		Window.__init__(self)
		self.set_title("Validador de CNH")
		base=VBox(False,False)
		fr=Frame("Numero CNH:")
		self.numero=Entry()
		fr.add(self.numero)
		bt=Button("Validar")
		base.pack_start(fr,False,True)
		base.pack_start(bt,False,True)
		self.add(base)
		bt.connect("clicked",lambda evt:self.validar())

	def validar(self):
		check=valida_cnh(self.numero.get_text())
		dlg=MessageDialog(parent=self,buttons=BUTTONS_OK)
		if check[0]:
			dlg.set_markup("<b>CNH Valida</b>")
		else:
			dlg.set_markup("<b>CNH invalida</b>\nDigito calculado %s"%check[2])
		dlg.run()
		dlg.destroy()
		

		


if __name__=="__main__":
	jn=ValidaCNH()
	jn.connect("destroy",main_quit)
	jn.show_all()
	main()

