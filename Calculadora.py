#
#-*-coding:utf-8-*-
#

from gtk import *
import datetime,time,math

class TaxaEquivalente(Frame):
    def __init__(self,parent):
        Frame.__init__(self)
        parent.desktop.append_page(self,Label("Taxa Equivalente"))
        base=VBox()
        fr1=Frame("Taxa origem:");base.pack_start(fr1,False,True)
        fr2=Frame("Taxa destino:");base.pack_start(fr2,False,True)
        self.origem=Entry()
        self.destino=Entry()
        fr1.add(self.origem)
        fr2.add(self.destino)
        b2=HBox()
        fro=Frame("Origem:")
        frd=Frame("Destino:")
        btr1=RadioButton(None,"Mensal")
        btr2=RadioButton(btr1,"Trimestral")
        btr3=RadioButton(btr2,"Semestral")
        btr4=RadioButton(btr3,"Anual")
        bto=[btr1,btr2,btr3,btr4]
        hb1=VBox(False,False)
        btr1=RadioButton(None,"Mensal")
        btr2=RadioButton(btr1,"Trimestral")
        btr3=RadioButton(btr2,"Semestral")
        btr4=RadioButton(btr3,"Anual")
        btd=[btr1,btr2,btr3,btr4]
        for b in bto:hb1.pack_start(b,False,True)
        fro.add(hb1)
        b2.pack_start(fro,True,True)
        hb1=VBox(False,False)
        for b in btd:hb1.pack_start(b,False,True)
        frd.add(hb1)
        b2.pack_start(frd,True,True)
        base.pack_start(b2)
        tb=Toolbar()
        bt1=ToolButton("gtk-execute")
        bt1.set_label("Converter")
        bt1.connect("clicked",lambda evt:self.calcular())
        tb.add(bt1)
        base.pack_end(tb,False,True)
        self.add(base)
        self.opt_origem=bto
        self.opt_destino=btd
        

    def calcular(self):
        tp_origem=0
        tp_destino=1
        for i  in range(0,4):
            if self.opt_origem[i].get_active():
                tp_origem=i
            if self.opt_destino[i].get_active():
                tp_destino=i
        valor=float(self.origem.get_text())
        pad=[30,90,180,360]#mes trimestre semestre ano        
        tp_destino=pad[tp_destino]
        tp_origem=pad[tp_origem]
        rs=0.00
        if tp_origem<tp_destino:
            rs = ((math.pow(((valor/100)+1),(tp_destino/tp_origem)))-1)*100;
        else: 
            rs = ((math.pow(((valor/100)+1),(1/(tp_origem/tp_destino))))-1)*100
        self.destino.set_text('%.8f'%rs)
#--------------------------------------------------------------------
class PReves(Frame):
	def __init__(self,parent):
		Frame.__init__(self)
		parent.desktop.append_page(self,Label("%T"))
		self.top=parent
		bx=VBox()
		self.add(bx)
		fr =Frame("Valor total:")
		fr1 =Frame("Valor parcial")
		fr2 = Frame("Resultado")
		tb=Toolbar()
		self.vt=Entry();fr.add(self.vt)
		self.vp=Entry();fr1.add(self.vp)
		self.res=Entry();fr2.add(self.res)
		bx.pack_start(fr,False,True)
		bx.pack_start(fr1,False,True)
		bx.pack_start(fr2,False,True)
		bx.pack_end(tb,False,True)
		bt=ToolButton("gtk-run")
		bt.set_label("Calcular")
		tb.add(bt)
		bt.connect("clicked",self.calcular)

	def calcular(self,evt=None):
		try:
			total=float(self.vt.get_text())
			parc=float(self.vp.get_text())
		except:
			dlg=MessageDialog(parent=self.parent.parent,buttons=BUTTONS_OK,type=MESSAGE_ERROR)
			dlg.set_markup("Erro convertendo valores");dlg.run()
			dlg.destroy()
			return
		self.res.set_text(str((parc*100l)/total))		
#==========================================================================================
class CalculadoraFita(Frame):
    def __init__(self):
        self._bt_pressed=lambda evt:self.key_pressed(evt.get_label())
        Frame.__init__(self)
        base=VBox(False,False)
        base.pack_end(self.teclado(),False,False)
        self.fita=TextView()
        self.fita.set_editable(False)
        self.fita.set_justification(JUSTIFY_RIGHT)
        self.x=Entry()
        self.x.set_text('0,00')
        self.y=Entry()        
        self.y.set_editable(False)
        self.x.connect("key_press_event",self.__chd)
        scw=ScrolledWindow()
        scw.add(self.fita)
        base.pack_start(scw,True,True)
        base.pack_start(self.y,False,True)
        base.pack_start(self.x,False,True)
        self.x.grab_focus()
        self.add(base)

    def __chd(self,*params):        
        key=gdk.keyval_name(params[1].keyval)
        if key[:3]=='KP_':key=key[3:]
        if key.lower()=='add':key='+'
        if key.lower()=='subtract':key='-'
        if key.lower()=='multiply':key='*'
        if key.lower()=='divide':key='/'
        if key.lower()=='separator':key='.'
        if key.lower()=='enter' or key.lower()=='return':key='='
        self.key_pressed(key)
        return(True)

    def key_pressed(self,key):        
        if key.lower()=='c':
            self.x.set_text("0.00")
        if key in ["+","-","*","/","="]:
            if key=='=':
                if self.y.get_text()=='0,00' or self.y.get_text().strip()=='':
                    self.fita.get_buffer().insert_at_cursor(self.x.get_text()+"+\n")
                    self.y.set_text(self.x.get_text())
                    self.x.set_text("0,00")
                else:
                    self.fita.get_buffer().insert_at_cursor(self.y.get_text()+' '+key+'\n---------------------------\n')            
                    self.x.set_text(self.y.get_text())
                    self.y.set_text('0,00')
            if key=='+':
                if self.y.get_text()=="":self.y.set_text("0,00")
                self.fita.get_buffer().insert_at_cursor(self.x.get_text()+' '+key+'\n')              
                self.y.set_text(('%.2f'%(float(self.x.get_text().replace(".","").replace(',','.'))
                                    +float(self.y.get_text().replace(".","").replace(',','.')))).replace('.',','))
                self.x.set_text('0,00')
            if key=='-':
                if self.y.get_text()=="":self.y.set_text("0,00")
                self.fita.get_buffer().insert_at_cursor(self.x.get_text()+' '+key+'\n')              
                y=float(self.y.get_text().replace(".","").replace(',','.'))
                x=float(self.x.get_text().replace(".","").replace(',','.'))
                self.y.set_text(('%.2f'%(y-x)).replace('.',','))
                self.x.set_text('0,00')
            if key=='/':
                if self.y.get_text()!='0,00' and self.y.get_text()!='':
                    self.fita.get_buffer().insert_at_cursor(self.x.get_text()+' '+key+'\n')              
                    y=float(self.y.get_text().replace(".","").replace(",","."))
                    x=float(self.x.get_text().replace(".","").replace(",","."))
                    ret=('%.2f'%(x/y)).replace(".",",")
                    self.x.set_text("0,00")
                    self.y.set_text(ret+" ")
            if key=='*':
                if self.y.get_text()!='0,00' and self.y.get_text()!='':
                    self.fita.get_buffer().insert_at_cursor(self.x.get_text()+' '+key+'\n')              
                    y=float(self.y.get_text().replace(".","").replace(",","."))
                    x=float(self.x.get_text().replace(".","").replace(",","."))
                    ret=('%.2f'%(x*y)).replace(".",",")
                    self.x.set_text("0,00")
                    self.y.set_text(ret+" ")
        if key in '1,2,3,4,5,6,7,8,9,0'.split(','):
            num=self.x.get_text()
            if float(self.x.get_text().replace(",","."))==0:
                num=float(key)/100
                self.x.set_text(('%.2f'%num).replace('.',','))
            else:
                num=(num.lstrip("0").replace(".","").replace(",","")+key)
                if len(num)<3:
                    num=((3-len(num))*'0')+num
                num=num[:-2]+","+num[-2:]
                self.x.set_text(num)
        if key.lower()=='backspace':
            num=self.x.get_text().replace(',','')[:-1]
            if len(num)<3:
                num=((3-len(num))*'0')+num
            self.x.set_text(num[:-2]+','+num[-2:])

    def teclado(self):
        base=Table(2,1)
        tbl=Table(3,4)
        bts=[Button('1'),Button('2'),Button('3'),Button('4'),Button('5'),Button('6'),Button('7'),Button('8'),Button('9'),Button('0'),Button("C")]
        for b in bts:b.connect("clicked",self._bt_pressed)
        pos=0
        for y in range(0,3):
            for x in range(0,3):
                    tbl.attach(bts[pos],x,x+1,y,y+1)
                    pos+=1
        tbl.attach(bts[9],0,2,4,5)
        tbl.attach(bts[10],2,3,4,5)
        tbl2=Table(1,5)
        posy=0
        for op in ["+","-","*","/","="]:
            bt=Button(op)
            bt.connect("clicked",self._bt_pressed)
            tbl2.attach(bt,0,1,posy,posy+1)            
            posy+=1        
        base.attach(tbl,0,2,0,1)
        base.attach(tbl2,2,3,0,1)
        return(base)
#==========================================================================================
class Somadias(Frame):
        def __init__(self,parent):
                Frame.__init__(self)
		parent.desktop.append_page(self,Label("Soma dias"))
                self.data=DatePicker()
                self.dias=SpinButton()
                self.dias.set_increments(1,10)
                self.dias.set_range(1,999)
                bt=ToolButton('gtk-execute')
                bt.connect("clicked",lambda evt:self.calcular())
                bx=VBox(False,False)
                fr1 = Frame("Data inicial:")
                fr2 = Frame("Dias:")
                bx.pack_start(fr1,False,True)
                bx.pack_start(fr2,False,True)
                bx.pack_start(bt,False,False)
                fr1.add(self.data);fr2.add(self.dias)
                self.add(bx)

        def calcular(self):
                try:
                        dia,mes,ano=map(int,self.data.get().split("/"))
                        data=datetime.date(ano,mes,dia)
                        data=data+datetime.timedelta(days=int(self.dias.get_value()))
                        res='%.2i/%.2i/%i'%(data.day,data.month,data.year)
                        dlg=MessageDialog(parent=self.parent.parent,buttons=BUTTONS_OK,type=MESSAGE_INFO)
                        dlg.set_markup('Data:\n'+res)
                        dlg.run();dlg.destroy()
                except Exception,e:
                        dlg=MessageDialog(parent=self.parent.parent,buttons=BUTTONS_OK,type=MESSAGE_ERROR)
                        dlg.set_markup('Erro no processamento\n'+str(e))
                        dlg.run();dlg.destroy()                        
                


class Calculadora(Window):
    def __init__(self):
        Window.__init__(self)
        self.set_title("Calculadoras")
        self.set_size_request(300,500)
        self.desktop=Notebook()
        self.add(self.desktop)
        self.desktop.append_page(CalculadoraFita(),Label("Calculadora"))
        PReves(self)
        Somadias(self)
        TaxaEquivalente(self)
		


        
class DatePicker(Frame):
    def __init__(self,text=''):
        if text=='':
            Frame.__init__(self)
        else:Frame.__init__(self,text)
        bx = HBox(False,0)
        self.__et =Entry()
        self.__et.set_editable(False)
        image = Image()
        image.set_from_pixbuf(gdk.pixbuf_new_from_xpm_data(["16 16 9 1",
                    " 	c None",
                    ".	c #000000",
                    "+	c #E012EA",
                    "@	c #8F599F",
                    "#	c #FFFFFF",
                    "$	c #9A2424",
                    "%	c #45C174",
                    "&	c #F9F609",
                    "*	c #48CFDB",
                    "                ",
                    " .............. ",
                    " .+++++++++++@. ",
                    " .@@@@@@@@@@@@. ",
                    " .............. ",
                    " .############. ",
                    " .#$#%$&#$%#$#. ",
                    " .##*###*##*##. ",
                    " .#$#%$&#$%#$#. ",
                    " .##*###*##*##. ",
                    " .#$#%$&#$%#$#. ",
                    " .##*###*##*##. ",
                    " .#$#%$&#$%#$#. ",
                    " .############. ",
                    " .............. ",
                    "                "]))
        bt =ToolButton(image)
        bt.connect("clicked",self.__set_date)
        bx.pack_start(self.__et,True,True)
        bx.pack_end(bt,False,False)
        self.add(bx)
        self.__et.set_text(time.strftime("%d/%m/%Y"))
        self.set_shadow_type(SHADOW_NONE)

    def set(self,data):
        ano,mes,dia = data.split('-')
        data = '%s/%s/%s'%(dia,mes,ano)
        self.__et.set_text(data)

    def get(self):
        return(self.get_date('d/m/y'))

    def get_date(self,format="d/m/y"):
        '''possible date formats d/m/y y-m-d '''
        dia,mes,ano=self.__et.get_text().split("/")
        return(format.replace("d",dia).replace("m",mes).replace("y",ano))
                
    def __set_date(self,evt):
        dlg=Dialog()
        dlg.set_title("Selecione a data:")
        dia,mes,ano = map(int,self.__et.get_text().split("/"))
        mes-=1       
        dlg.add_button("gtk-ok",1)
        dlg.add_button("gtk-cancel",0)
        cld=Calendar()
        cld.select_day(dia)
        cld.select_month(mes,ano)
        dlg.vbox.add(cld);dlg.show_all();
        if dlg.run():
            ano,mes,dia =cld.get_date()
            mes+=1
            self.__et.set_text("%.2i/%.2i/%s"%(dia,mes,ano))
        dlg.destroy()

def formata_moeda(valor):
    inteiro,decimal = valor.split(".")
    inteiro = split1000(inteiro)
    return('%s,%s'%(inteiro,decimal))

def split1000(s):
   return s if len(s) <= 3 else split1000(s[:-3]) + '.' + s[-3:] 


if __name__=="__main__":
	w=Calculadora()
	w.connect("destroy",main_quit)
	w.show_all()
	main()
