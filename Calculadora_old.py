#
#-*-coding:utf-8-*-
#
from gtk import *
import  pygtk, gobject, string ,sys
#-------------------------------------------------------------------------------------------------
class FloatEntry(Entry):
    def __init__(self,top):
        Entry.__init__(self)
        self.comando=top.comando
        self.connect("insert_text",self.validar)
        self.connect("key_press_event",self.testa_comandos)

    def testa_comandos(self,entry,evento):#Teclas de atalho
        keyname = gdk.keyval_name(evento.keyval)
        if keyname=='c' or keyname=='C':self.set_text('')
        if keyname=='KP_Enter' or keyname=='Return':self.comando('=')
        
    def validar(self,entry,text,*args):      
        entry.stop_emission("insert_text")#evita o processamento normal do sinal
        if text in '+-*/':return(self.comando(text))
        result = ''.join([c for c in text if c in '0123456789'])
        texto=self.formata((entry.get_text()+result))
        entry.handler_block_by_func(self.validar)#Bloqueia o sinal para evitar recursividade
        entry.set_text(texto)
        entry.handler_unblock_by_func(self.validar)

    def formata(self,texto):
        texto=texto.replace(",","").replace(".","")
        if texto=='':texto=0
        texto=texto.lstrip('0').zfill(4)
        texto=self.split1000(texto[:-2])+','+texto[-2:]#separador de milhar
        return(texto)

    def split1000(self,s):return s if len(s) <= 3 else self.split1000(s[:-3]) + '.' + s[-3:] 

#-------------------------------------------------------------------------------------------------
class Teclado(Frame):
    def __init__(self,entry):
        Frame.__init__(self)
        self.entry=entry
        bts =map(lambda num:Button('%i'%num),range(1,10))
        bts.append(Button('0'))
        ct = Table(4,1)
        gr = Table(4,3)
        contador=0
        comandos=map(lambda cmd:Button(cmd),['+','-','*','/','=','C'])
        gr2 = Table(1,6)
        for x in range(0,6):gr2.attach(comandos[x],0,1,x,x+1)
        for y in range(0,3):        
            for x in range(0,3):
                gr.attach(bts[contador],x,x+1,y,y+1)
                contador+=1
        gr.attach(bts[9],0,3,3,4)
        ct.attach(gr,0,3,0,1)
        ct.attach(gr2,3,4,0,1)
        self.add(ct)
        for bt in bts:bt.connect('clicked',lambda botao:entry.insert_text(botao.get_label(),entry.get_position()))
        for bt in comandos:bt.connect('clicked',self.comandos)

    def comandos(self,bt):
        comando=bt.get_label().lower()
        if comando=='c':return(self.entry.set_text(''))
        return(self.entry.comando(comando))
        
       
#-------------------------------------------------------------------------------------------------
class Calculadora(Window):
    def __init__(self):
        Window.__init__(self)
        img =gdk.pixbuf_new_from_xpm_data(icone)
        self.set_icon(img)
        self.set_title("Calculadora")
        self.fita=TextView()
        scw = ScrolledWindow()
        scw.set_policy(POLICY_AUTOMATIC,POLICY_AUTOMATIC)
        scw.add(self.fita)
        self.fita.set_editable(False)
        self.fita.set_justification(JUSTIFY_RIGHT)
        self.fita.set_size_request(-1,200)
        bx = VBox(False,False);self.add(bx)
        self.y=Entry()
        self.y.set_editable(False)
        self.x=FloatEntry(self)
        self.set_focus(self.x)
        btlimpa=Button("")
        btlimpa.child.set_markup("<small>Limpa a fita</small>")
        btlimpa.connect("clicked",lambda evt:
            self.fita.get_buffer().delete(self.fita.get_buffer().get_start_iter(),self.fita.get_buffer().get_end_iter()))
        bx.pack_start(btlimpa,False,True)
        bx.pack_start(scw,True,True)
        bx.pack_start(self.y,False,True)
        bx.pack_start(self.x,False,True)
        teclado=Teclado(self.x)
        bx.pack_start(teclado,True,False)
        self.show_all()
        self.x.grab_focus()

    def comando(self,op):
        if op=='=':
            self.fita.get_buffer().insert_at_cursor(self.y.get_text()+'\n')   
            self.fita.get_buffer().insert_at_cursor((35*'-')+'\n')
#            self.x.set_text('0')	
            self.y.set_text('0')
            return
        if self.x.get_text().strip()=='':self.x.set_text("0")
        x = float(self.x.get_text().replace('.','').replace(",","."))
        if x==0:return #cancela se o usuario tentar fazer alguma operacao com 0 evita erros de digitação
        y=0
        if self.y.get_text().strip()!='':y=float(self.y.get_text().replace('.','').replace(",","."))
        if op=='+':y=x+y
        if op=='-':y=y-x
        if op=='*':
            if y==0:
                y=x
            else:
                y=y*x        
        if op=='/':
            if y==0:
                y=x
            else:
                y=y/x     
        self.fita.get_buffer().insert_at_cursor(self.x.get_text()+' '+op+'\n')   
        self.y.set_text(self.x.formata(str('%.2f'%y)))
        self.x.set_text('0')
#-------------------------------------------------------------------------------------------------
icone=["16 16 3 1",
" 	c None",
".	c #000000",
"+	c #938888",
"  ............  ",
"  .          .  ",
"  . ++++++++ .  ",
"  . ++++++++ .  ",
"  .          .  ",
"  ............  ",
"   .       .    ",
"   . . . . .    ",
"   .       .    ",
"   . . . . .    ",
"   .       .    ",
"   . . . . .    ",
"   .       .    ",
"   . . ... .    ",
"   .       .    ",
"   .........    "]

if __name__=="__main__":
    jn=Calculadora()
#    jn.show_all()
    jn.connect("destroy",main_quit)    
    main()
