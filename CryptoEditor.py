#
#-*-coding:utf-8-*-

from gtk import *
import aes
from hashlib import md5

class CryptoEditor(Window):
    def __init__(self):
        Window.__init__(self)
        self.set_icon(gdk.pixbuf_new_from_xpm_data(self.icone()))
        self.set_title("Ferramenta de criptografia")
        bx=VBox(False,0)        
        bx.pack_start(self.make_menu(),False,True)
        tb = Toolbar()
        bx.pack_start(tb,False,True)
        self.make_toolbar(tb)
        fr=Frame("Senha:")
        self.senha=Entry()
        fr.add(self.senha)
        bx.pack_start(fr,False,True)
        fr = Frame("Metodo:")
        self.metodo=combo_box_new_text()
        self.metodo.append_text("AES")
        self.metodo.append_text("XOR")        
        self.metodo.set_active(0)
        fr.add(self.metodo)
        bx.pack_start(fr,False,True)
        self.senha.set_visibility(False)
        scw = ScrolledWindow()
        scw.set_policy(POLICY_AUTOMATIC,POLICY_AUTOMATIC)
        self.texto=TextView()
        scw.add(self.texto)
        bx.pack_start(scw,True,True)
        self.set_size_request(400,500)
        self.add(bx)
        self.show_all()

    def get_text(self):
        bf = self.texto.get_buffer()
        ini =bf.get_start_iter()
        end = bf.get_end_iter()
        return(bf.get_text(ini,end))
        

    def make_menu(self):
        M=MenuBar()
        m=Menu()
        m1 = ImageMenuItem("gtk-open");m1.connect("activate",self.abrir)
        m2 = ImageMenuItem("gtk-save");m2.connect("activate",self.salvar)    
        m3 = ImageMenuItem("gtk-close");m3.connect("activate",lambda evt:self.destroy())
        m.append(m1)
        m.append(m2)
        m.append(SeparatorMenuItem())
        m.append(m3)
        mm=MenuItem("Crypto")
        mm.set_submenu(m)
        M.add(mm)
        return(M)

    def make_toolbar(self,tb):
        bt=[ToolButton("gtk-close"),SeparatorToolItem(),ToolButton("gtk-execute"),ToolButton("gtk-justify-center")]
        bt[0].connect("clicked",lambda evt:self.destroy())
        bt[2].set_label("Criptografar")
        bt[2].connect("clicked",lambda evt:self.encrypt())        
        bt[3].set_label("Decriptografar")        
        bt[3].connect("clicked",lambda evt:self.decrypt())                
        for button in bt:tb.add(button)
        
    def aesc(self):
        senha = md5(self.senha.get_text()).hexdigest()
        self.texto.get_buffer().set_text(aes.encryptData(senha,self.get_text()).encode("base64"))

    def aesd(self):
        senha = md5(self.senha.get_text()).hexdigest()
        self.texto.get_buffer().set_text(aes.decryptData(senha,self.get_text().decode("base64")))

    def encrypt(self):
        if(self.metodo.get_active()==0):
            self.aesc()
        else:
            self.texto.get_buffer().set_text(self.sxor(self.senha.get_text(),self.get_text()))

    def decrypt(self):
        if(self.metodo.get_active()==0):
            self.aesd()
        else:self.texto.get_buffer().set_text(self.sxor(self.senha.get_text(),self.get_text().decode('base64')).decode("base64"))
        
    def sxor(self,chave,valor):
        idx=0
        ret=''
        contador=len(chave)-1
        for c in valor:
            ret+=chr(ord(c)^ord(chave[idx]))
            if idx<contador:
                     idx+=1
            else:idx=0                    
        return(ret.encode('base64'))

    def abrir(self,evt):
        dlg=FileChooserDialog(title="Abrir", parent=self, action=FILE_CHOOSER_ACTION_OPEN, buttons=("gtk-yes",1,"gtk-no",0))
        try:
            if dlg.run():self.texto.get_buffer().set_text(open(dlg.get_filename()).read())
        except Exception,e:
            dlg.destroy()
            dlg=MessageDialog(parent=self,buttons=BUTTONS_OK,type=MESSAGE_ERROR);dlg.set_markup(str(e))
            dlg.run();dlg.destroy()
        dlg.destroy()        
        
    def salvar(self,evt):
        dlg=FileChooserDialog(title="Abrir", parent=self, action=FILE_CHOOSER_ACTION_SAVE, buttons=("gtk-yes",1,"gtk-no",0))
        try:
            if dlg.run():open(dlg.get_filename(),'w').write(self.get_text())
        except Exception,e:
            dlg.destroy()
            dlg=MessageDialog(parent=self,buttons=BUTTONS_OK,type=MESSAGE_ERROR);dlg.set_markup(str(e))
            dlg.run();dlg.destroy()
        dlg.destroy()        

    def icone(self):
        return(["16 16 39 1",
                " 	c None",
                ".	c #000000",
                "+	c #B06041",
                "@	c #BE8D71",
                "#	c #A2715A",
                "$	c #9F734B",
                "%	c #8C5A31",
                "&	c #8C5B31",
                "*	c #B95F27",
                "=	c #B17E5D",
                "-	c #AC7B67",
                ";	c #C18E63",
                ">	c #8D5B31",
                ",	c #8C5B32",
                "'	c #BD6F06",
                ")	c #B15C3E",
                "!	c #BC8B6E",
                "~	c #9F6E58",
                "{	c #A57B52",
                "]	c #8D5A32",
                "^	c #9E5D05",
                "/	c #BB6124",
                "(	c #AE7C59",
                "_	c #B3816C",
                ":	c #BB8861",
                "<	c #8C5A32",
                "[	c #92580B",
                "}	c #B86C06",
                "|	c #B1573A",
                "1	c #BA886B",
                "2	c #9B6B57",
                "3	c #AB825A",
                "4	c #AB6A1F",
                "5	c #9A5B05",
                "6	c #BC651F",
                "7	c #AE7452",
                "8	c #BA8870",
                "9	c #B4815E",
                "0	c #8D5A31",
                "                ",
                "                ",
                "                ",
                "      .....     ",
                "      .   .     ",
                "      .   .     ",
                "      .   .     ",
                "     +@#$%&%    ",
                "     *=-;>&,    ",
                "     ')!~{&]    ",
                "     ^/(_:%<    ",
                "     [}|123&    ",
                "     4567890    ",
                "                ",
                "                ",
                "                "])

if __name__=="__main__":
    jn=CryptoEditor()
    jn.connect("destroy",main_quit)
    jn.show()    
    main()
    
